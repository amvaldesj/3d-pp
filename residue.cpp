#include <cstring>
#include <vector>

#include <iostream>
using namespace std;
#include <bits/stdc++.h>
#include "structures.h"
#include "residue.h"
#include "atom.h"

/**/
void print_residues(vector<Residue> residues) {
    for (Residue residue: residues) {
        cout << "\t" << residue.name << ":" << residue.res_id;
        cout << "[" << residue.atoms.size() << "] [";
        cout << residue.geom_center[0] << ", " << residue.geom_center[1] << ", " << residue.geom_center[2] << "] ";
        cout << endl;
        print_atoms(residue.atoms);
    }
}

/**/
void calc_geom_center_group_r(Residue *residue) {
    float *coord = new float[3];
    coord[0] = coord[1] = coord[2] = 0.0;
        
    if (strcmp(residue->name.c_str(), "GLY") == 0) {
        for (Atom atom: residue->atoms) {
            if (strcmp(atom.name.c_str(), " CA ") == 0) {
                coord[0] = atom.coord[0];
                coord[1] = atom.coord[1];
                coord[2] = atom.coord[2];
                break;
            }
        }
    }  else {
        int c = 0;
        float coordx = 0.0;
        float coordy = 0.0;
        float coordz = 0.0;
        
        for (Atom atom: residue->atoms) {
            if (strcmp(atom.name.c_str(), " N  ") != 0 && 
                strcmp(atom.name.c_str(), " CA ") != 0 && 
                strcmp(atom.name.c_str(), " C  ")  != 0 && 
                strcmp(atom.name.c_str(), " O  ")  != 0 && 
                strcmp(atom.name.c_str(), " H  ")  != 0) {
                    
                c++;
                coordx += atom.coord[0];
                coordy += atom.coord[1];
                coordz += atom.coord[2];
            }
        }

        coord[0] = (coordx/c);
        coord[1] = (coordy/c);
        coord[2] = (coordz/c);
    }
    
    residue->geom_center[0] = coord[0];
    residue->geom_center[1] = coord[1];
    residue->geom_center[2] = coord[2];
}

/**/
Atom *get_atom(Residue residue, string name) {
    vector<Atom> atoms = residue.atoms;
    Atom *atom = NULL;
    
    for (long unsigned int i=0; i<atoms.size(); i++) {
        if (strcmp(atoms[i].name.c_str(), name.c_str()) == 0) {
            atom = &atoms[i];
            break;
        }
    }
    
    return atom;
}
