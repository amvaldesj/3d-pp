#include <vector>
#include <omp.h>

#include <iostream>
using namespace std;
#include <bits/stdc++.h>
#include "structures.h"
#include "site.h"
#include "pattern.h"
#include "rmsd.h"

//
void cluster_sites_version1(parameters params, vector<Pattern> &all_patterns) {
    cout << "clustering sites... " << endl;
    auto start = chrono::system_clock::now();
        
    //
    int n_patterns = all_patterns.size();
    
    #pragma omp parallel for schedule(runtime) num_threads(params.nthreads)
    for (int i=0; i<n_patterns; i++) {

#ifdef TRACING
        Extrae_event(400, i+1);
#endif
        // sorts sites on the Pattern, by str_site, chain and protein.
        sort(all_patterns[i].sites.begin(), all_patterns[i].sites.end(), 
            [ ](const Site s1, const Site s2) {
            return tie(s1.str_site, s1.chain, s1.protein) < tie(s2.str_site, s2.chain, s2.protein);
            }
        );

#ifdef TRACING
        Extrae_event(400, 0);
#endif

        // clustering.
#ifdef TRACING
        Extrae_event(401, i+1);
#endif
        // sites in this pattern.
        int n_sites = all_patterns[i].sites.size();
        
        //
        for (int s=0; s<n_sites; s++) {
            
            // clusters in this pattern.
            int n_cluster = all_patterns[i].clusters.size();
            bool matched = false;
            Site site_cluster;
            float rmsd = 0;
            string prot = "\0";
                
            int n_res = all_patterns[i].sites[s].residues.size();
            float array_site [n_res*5][3];
            create_array_coordinates_to_rmsd(&all_patterns[i].sites[s], array_site);
                
            // compare site with the first site of cluster(s). calculate RMSD to match.
            for (int c=0; c<n_cluster && matched == false; c++) {
                site_cluster = all_patterns[i].clusters[c].sites[0];
                    
                int n_res = site_cluster.residues.size();
                float array_site_cluster [n_res*5][3];
                create_array_coordinates_to_rmsd(&site_cluster, array_site_cluster);
                    
                float mov_com[3];
                float mov_to_ref[3];
                float U[3][3];
                // rotate and calculate rmsd.
                calculate_rotation_rmsd(array_site, array_site_cluster, n_res*5, mov_com, mov_to_ref, U, &rmsd);

                // site matches this cluster.
                if (rmsd <= params.rmsd) {
                    all_patterns[i].sites[s].rmsd = rmsd;
                    all_patterns[i].clusters[c].sites.emplace_back(all_patterns[i].sites[s]);

                    // the cluster already in this protein?
                    prot = all_patterns[i].sites[s].protein;
                        
                    const auto protein = std::find_if (
                        all_patterns[i].clusters[c].in_protein.begin(),
                        all_patterns[i].clusters[c].in_protein.end(),
                        [prot](const string &p) {
                            return p == prot;
                        }
                    );
                
                    //first time this protein?
                    if (protein == all_patterns[i].clusters[c].in_protein.end()) {
                        all_patterns[i].clusters[c].in_protein.emplace_back(
                            all_patterns[i].sites[s].protein);
                    }

                    matched = true;
                }
            }
                
            // the site does not match in any cluster.
            if (matched == false) {
                all_patterns[i].sites[s].base = true;
                    
                // creates new cluster.
                string cluster_name = all_patterns[i].name + "-" + to_string(all_patterns[i].clusters.size() + 1);
                    
                Cluster cluster;
                cluster.name = cluster_name;
                cluster.sites = {};
                cluster.sites.emplace_back(all_patterns[i].sites[s]);
                cluster.in_protein.emplace_back(all_patterns[i].sites[s].protein);
                    
                all_patterns[i].clusters.emplace_back(cluster);
            }
        }
#ifdef TRACING
        Extrae_event(401, 0);
#endif
    }
    
    //
    auto end = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = end-start;
    cout << elapsed_seconds.count() << " seconds" << endl;
}

//
void cluster_sites_version2(parameters params, set<Pattern> &set_patterns) {
    cout << "clustering sites... " << endl;
    auto start = chrono::system_clock::now();
    
#ifdef TRACING
        Extrae_event(401, 1);
#endif
    // https://stackoverflow.com/questions/2513988/iteration-through-std-containers-in-openmp
    #pragma omp parallel num_threads(params.nthreads) 
    {
        // only for traces...
        int id = omp_get_thread_num();
        
#ifdef TRACING
    Extrae_event(401, id+1);
#endif
        
        for (set<Pattern>::iterator it = set_patterns.begin(); it != set_patterns.end(); ++it) {
            #pragma omp single nowait
            {

#ifdef TRACING
        Extrae_event(400, id+1);
#endif
                // sorts sites on the Pattern, by str_site, chain and protein.
                sort(it->sites.begin(), it->sites.end(), [ ](const Site s1, const Site s2) {
                        return tie(s1.str_site, s1.chain, s1.protein) < tie(s2.str_site, s2.chain, s2.protein);
                    }
                );
#ifdef TRACING
        Extrae_event(400, 0);
#endif
                // sites in this pattern.
                int n_sites = it->sites.size();
                
                //
                for (int s=0; s<n_sites; s++) {
                    
                    // clusters in this pattern.
                    int n_cluster = it->clusters.size();
                    bool matched = false;
                    Site site_cluster;
                    float rmsd = 0;
                    string prot = "\0";
                        
                    int n_res = it->sites[s].residues.size();
                    float array_site [n_res*5][3];
                    create_array_coordinates_to_rmsd(&it->sites[s], array_site);
                        
                    // compare site with the first site of cluster(s). calculate RMSD to match.
                    for (int c=0; c<n_cluster && matched == false; c++) {
                        site_cluster = it->clusters[c].sites[0];
                            
                        int n_res = site_cluster.residues.size();
                        float array_site_cluster [n_res*5][3];
                        create_array_coordinates_to_rmsd(&site_cluster, array_site_cluster);
                            
                        float mov_com[3];
                        float mov_to_ref[3];
                        float U[3][3];
                        // rotate and calculate rmsd.
                        calculate_rotation_rmsd(array_site, array_site_cluster, n_res*5, mov_com, mov_to_ref, U, &rmsd);

                        // site matches this cluster.
                        if (rmsd <= params.rmsd) {
                            it->sites[s].rmsd = rmsd;
                            it->clusters[c].sites.emplace_back(it->sites[s]);

                            // the cluster already in this protein?
                            prot = it->sites[s].protein;
                                
                            const auto protein = std::find_if (
                                it->clusters[c].in_protein.begin(),
                                it->clusters[c].in_protein.end(),
                                [prot](const string &p) {
                                    return p == prot;
                                }
                            );
                        
                            //first time this protein?
                            if (protein == it->clusters[c].in_protein.end()) {
                                it->clusters[c].in_protein.emplace_back(
                                    it->sites[s].protein);
                            }

                            matched = true;
                        }
                    }
                        
                    // the site does not match in any cluster.
                    if (matched == false) {
                        it->sites[s].base = true;
                            
                        // creates new cluster.
                        string cluster_name = it->name + "-" + to_string(it->clusters.size() + 1);
                            
                        Cluster cluster;
                        cluster.name = cluster_name;
                        cluster.sites = {};
                        cluster.sites.emplace_back(it->sites[s]);
                        cluster.in_protein.emplace_back(it->sites[s].protein);
                            
                        it->clusters.emplace_back(cluster);
                    }
                }
            }
        }
#ifdef TRACING
        Extrae_event(401, 0);
#endif
    }
#ifdef TRACING
        Extrae_event(401, 0);
#endif

    //
    auto end = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = end-start;
    cout << elapsed_seconds.count() << " seconds" << endl;
}
