// Instrumentation framework.
#ifdef TRACING
#include <extrae.h>
// in power9:
// module load gcc/8.1.0
// module load EXTRAE/4.0.0
//#include "/apps/BSCTOOLS/extrae/4.0.0/openmpi_3_1_0/include/extrae.h"
#endif

struct Atom {
    int serial;
    string name;
    float coord[3];
};

struct Residue {
    int res_id;
    string name;
    float geom_center[3];
    vector<Atom> atoms = {};
};

struct Site {
    string chain;
    string protein;
    string pattern;
    string str_site;
    float rmsd = 0;
    float (*array_site)[3]; //[n_res*5][3]; // for measuring rmsd.
    vector<Residue> residues = {};
    bool base;
    string vcoord;
};

struct Cluster {
    string name;
    vector<Site> sites = {};
    vector<string> in_protein = {};
};

struct Pattern {
    string name;
    // mutable, allows update in the set().
    mutable vector<Site> sites = {};
    mutable vector<Cluster> clusters = {};
    mutable vector<string> in_protein = {};
    mutable float coverage;
    
    // for comparison on set().
    bool operator < (const Pattern &other) const {
        return name < other.name;
    }
};

struct Chain {
    string name;
    string protein;
    vector<Residue> residues = {};
    vector<Site> sites = {};
    vector<Pattern> patterns = {};
    struct kdtree *ptree = NULL;
    float xmin;
    float ymin;
    float zmin;
    float xmax;
    float ymax;
    float zmax;
};

struct parameters {
    float step;
    float radius;
    float rmsd;
    float coverage;
    float extend;
    int    nres;
    int    nthreads;
    string pdb_list_file;
    string dirout = "results";
    string dirpdbs;
};
