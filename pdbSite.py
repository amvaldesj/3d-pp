#!/usr/bin/env python
# -*- coding: utf-8 -*-

from Bio.PDB import PDBIO
from Bio.PDB.Structure import Structure 
from Bio.PDB.Model import Model 
from Bio.PDB.Chain import Chain 

class PDBSite(object):
  def __init__(self, chain, filename, residues):
    self._residues = residues
    self._filename = filename
    self._struct = Structure (self._filename)
    self._model = Model(0)
    self._chain = Chain(chain)

    #
    self._struct.add(self._model)
    self._model.add(self._chain)
    
    #
    self._addResidues()
    self._writeFile()
  
  def _addResidues (self):
    for r in self._residues:
      self._chain.add(r)
  
  def _writeFile(self):
    io = PDBIO()
    io.set_structure(self._struct)
    io.save(self._filename)
