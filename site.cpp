#include <omp.h>
#include <map>
#include <algorithm> // std::find_if
#include <iostream>
using namespace std;
#include <bits/stdc++.h>
#include "structures.h"
#include "site.h"
#include "residue.h"

/**/
map<string,string> map_res3to1 = {{"ALA","A"}, {"ARG","R"}, {"ASN","N"}, {"ASP","D"}, {"CYS","C"}, 
                                  {"GLN","Q"}, {"GLU","E"}, {"GLY","G"}, {"HIS","H"}, {"ILE","I"}, 
                                  {"LEU","L"}, {"LYS","K"}, {"MET","M"}, {"PHE","F"}, {"PRO","P"}, 
                                  {"SER","S"}, {"THR","T"}, {"TRP","W"}, {"TYR","Y"}, {"VAL","V"}};

/**/
Site create_site(Chain *chain, list<int> neigh_list, string vcoord) {
    string str_site = "\0";

    Site new_site;
    new_site.chain = chain->name;
    new_site.protein = chain->protein;
    new_site.residues = {};
    new_site.pattern = "\0";
    new_site.base = false;
    new_site.vcoord = vcoord; 
    
    //
    for (int res_id: neigh_list) {
        
        // gets Residue object.
        const auto residue = std::find_if (
            chain->residues.begin(),
            chain->residues.end(),
            [res_id](const Residue &res) {
                return res.res_id == res_id; 
            }
        );
        
        if (residue != chain->residues.end()) {
            if (str_site == "\0")
                str_site = str_site + residue->name + to_string(residue->res_id);
            else
                str_site = str_site + ":" + residue->name + to_string(residue->res_id);
                
            new_site.residues.emplace_back(*residue);
        }
    }
    
    /*
    //
    int n_res = new_site.residues.size();
    float array[n_res*5][3];
    create_array_coordinates_to_rmsd(&new_site, array);
    new_site.array_site = array;
    */
    
    //
    new_site.str_site = str_site;

    bool is_valid = create_pattern(new_site);
    if (is_valid == false )
        new_site.residues = {};
    
    return new_site;
}

/**/
bool create_pattern(Site &site) {
    bool is_valid = true;
    string pattern = "\0";
    map<string,string>::iterator it;
    map<string,int>::iterator it_acc;
    map<string,int> map_res_acc = {{"A",0}, {"C",0}, {"D",0}, {"E",0}, {"F",0}, 
                                   {"G",0}, {"H",0}, {"I",0}, {"K",0}, {"L",0}, 
                                   {"M",0}, {"N",0}, {"P",0}, {"Q",0}, {"R",0}, 
                                   {"S",0}, {"T",0}, {"V",0}, {"W",0}, {"Y",0}};
                                    
    for (Residue residue: site.residues) {
        it = map_res3to1.find(residue.name);
        // residue name (3 letters) exists in the map?
        if (it != map_res3to1.end()) {
            it_acc = map_res_acc.find(it->second);
            
            // residue name (1 letter) exists in the map?
            if (it_acc != map_res_acc.end()) {
                it_acc->second += 1;
            } else {
                is_valid = false;
                break;
            }
        } else {
            is_valid = false;
            break;
        }
    }
    
    if (is_valid == true) {
        // creates de pattern.
        for (it_acc = map_res_acc.begin(); it_acc != map_res_acc.end(); ++it_acc) {
            if (it_acc->second > 0) {
                pattern = pattern + to_string(it_acc->second) + it_acc->first;
            }
        }
        
        site.pattern = pattern;
    }

    return is_valid;
}

/**/
void merge_all_patterns(vector<Pattern> tmp_patterns, vector<Pattern> &all_patterns) {
    //
    string pattern_string;
    // number of patterns in all_chains[c].
    long unsigned int n_patterns = tmp_patterns.size();

    for (long unsigned int p=0; p<n_patterns; p++) {
        pattern_string = tmp_patterns[p].name;
        
        // find for the pattern in the vector all_patterns.
        vector<Pattern>::iterator pattern = std::find_if (
            all_patterns.begin(),
            all_patterns.end(),
            [pattern_string](const Pattern &p) {
                return p.name == pattern_string;
            }
        );
            
        // patterns exists?
        if (pattern == all_patterns.end()) {
            // adds pattern.
            all_patterns.emplace_back(tmp_patterns[p]);
                
        } else {
            // pattern found. adds sites to the patterns.
            pattern->sites.insert(pattern->sites.end(), 
                tmp_patterns[p].sites.begin(), 
                tmp_patterns[p].sites.end()
            );
                
            // adds proteins to patterns.
            pattern->in_protein.insert(pattern->in_protein.end(), 
                tmp_patterns[p].in_protein.begin(), 
                tmp_patterns[p].in_protein.end());
        }
    }
}

/**/
void create_array_coordinates_to_rmsd(Site *site, float array[][3]) {
    Atom *atom;
    
    int i = 0;
    for (Residue residue: site->residues) {
        array[i][0] = residue.geom_center[0];
        array[i][1] = residue.geom_center[1];
        array[i][2] = residue.geom_center[2];
        
        Atom *atomCA = get_atom(residue, " CA ");

        if (atomCA == NULL) {
            /*
            cerr << "Warning: protein " << site->protein << ", chain " << site->chain << endl;
            cerr << " residue " << residue.name << ":" << residue.res_id << " without atom CA" << endl;
            cerr << "Using geometrical center" << endl;
            */
            array[i+1][0] = residue.geom_center[0];
            array[i+1][1] = residue.geom_center[1];
            array[i+1][2] = residue.geom_center[2];
        } else {
            array[i+1][0] = atomCA->coord[0];
            array[i+1][1] = atomCA->coord[1];
            array[i+1][2] = atomCA->coord[2];
        }
        
        atom = get_atom(residue, " N  ");
        if (atom == NULL) {
            /*
            cerr << "Warning: protein " << site->protein << ", chain " << endl;
            cerr << site->chain << " residue " << residue.name << ":" << residue.res_id << " without atom N" << endl;
            cerr << "Using geometrical center" << endl;
            */
            array[i+2][0] = residue.geom_center[0];
            array[i+2][1] = residue.geom_center[1];
            array[i+2][2] = residue.geom_center[2];
        } else {
            array[i+2][0] = atom->coord[0];
            array[i+2][1] = atom->coord[1];
            array[i+2][2] = atom->coord[2];
        }
        
        atom = get_atom(residue, " C  ");
        if (atom == NULL) {
            /*
            cerr << "Warning: protein " << site->protein << ", chain " << endl;
            cerr << site->chain << " residue " << residue.name << ":" << residue.res_id << " without atom C" << endl;
            cerr << "Using geometrical center" << endl;
            */
            array[i+3][0] = residue.geom_center[0];
            array[i+3][1] = residue.geom_center[1];
            array[i+3][2] = residue.geom_center[2];
        } else {
            array[i+3][0] = atom->coord[0];
            array[i+3][1] = atom->coord[1];
            array[i+3][2] = atom->coord[2];
        }
        
        atom = get_atom(residue, " H  ");
        if (atom == NULL) {
            /*
            cerr << "Warning: protein " << site->protein << ", chain " << endl;
            cerr << site->chain << " residue " << residue.name << ":" << residue.res_id << " without atom H" << endl;
            cerr << "Using geometrical center" << endl;
            */
            array[i+4][0] = residue.geom_center[0];
            array[i+4][1] = residue.geom_center[1];
            array[i+4][2] = residue.geom_center[2];
        } else {
            array[i+4][0] = atom->coord[0];
            array[i+4][1] = atom->coord[1];
            array[i+4][2] = atom->coord[2];
        }
        
        i += 5;
    }
}
