#!/usr/bin/env python
#-*- coding: utf-8 -*-

import numpy as np
import operator

class Pattern():
  def __init__(self, name, diffAngle, percSim):
    self.name = name
    self.index = 0
    self.dcluster = {}
    self._diffAngle = diffAngle
    self._percSim = percSim
  
  def addCluster(self, namePatternCluster, oPatternCluster):
    self.dcluster[namePatternCluster] = oPatternCluster
  
  def getPatternCluster(self, key):
    return self.dcluster[key]
  
  """
  Realiza la diferencia entre los valores de los ángulos del sitio base de los cluster del patrón y los del nuevo sitio.
  Retorna el cluster donde calza el sitio.
  """
  def matchCluster(self, angles):
    scores = {}
    
    # recorre cada una de los cluster asociadas al patrón.
    for nameCluster, cluster in self.dcluster.iteritems():
      res = {}
      nAngles = 0
      nAnglesMin = 0
      
      # recorre cada ángulo del sitio base del cluster y los resta con los correspondientes ángulos del nuevo sitio.
      # cada ángulo es identificado por su key.
      for key, value in cluster.site.getAngles().iteritems():
          
        res[key] = np.absolute(np.subtract(value, angles[key]))
        
        # evalua si la diferencia entre los ángulos cumple el filtro ingresado.
        # contabiliza los ángulos evaluados y los que cumplen con el filtro.
        for r in res[key]:
          nAngles = nAngles + 1
          if r <= self._diffAngle:
            nAnglesMin = nAnglesMin + 1

      # calcula porcentaje.
      perc = (100 * nAnglesMin)/nAngles
      
      # evalua si cumple con el procentaje mínimo de similitud ingresado.
      if perc >= self._percSim:
        # si cumple, almacena el nombre key del cluster y el porcentaje en el diccionario.
        scores[cluster.name] = perc
    
    # si calza en algun cluster.
    # cuando calza en más de uno retorna el de máximo porcentaje.
    if len(scores)>0:
      return max(scores.iteritems(), key=operator.itemgetter(1))[0]
    else:
      return None
