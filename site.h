#include <list>
/**/
Site create_site(Chain *chain, list<int> neigh_list, string vcoord);
/**/
bool create_pattern(Site &site);
/**/
void merge_all_patterns(vector<Pattern> tmp_patterns, 
    vector<Pattern> &all_patterns);
/**/
void create_array_coordinates_to_rmsd(Site *site, float array[][3]);
