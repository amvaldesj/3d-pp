#include <vector>
#include <iostream>
using namespace std;

#include "structures.h"
#include "atom.h"

/**/
void print_atoms(vector<Atom> atoms) {
    cout << "\t\t";
    for (Atom atom: atoms) {
        cout << atom.name << ":" << atom.serial << "[" << atom.coord[0] << ", " << atom.coord[1] << ", " << atom.coord[2] << "]";
    }
    cout << endl;
}
