#include <cstring>
#include <vector>
#include <list>
#include <fstream>

#include <iostream>
using namespace std;
#include <bits/stdc++.h>
#include "kdtree/kdtree.h"
#include "structures.h"
#include "chain.h"
#include "residue.h"
#include "site.h"
#include "utils.h"

/**/
vector<Chain> parse_pdb(string pdb_id, parameters params) {
    ifstream inFile;
    char line[100];

    // variables from the PDB file
    int	serial;
    char atom_name[5];
    char res_name[4];
    char res_seq_plus_iCode[6];
    float coord_x ,coord_y ,coord_z;
    char chain[2] = "\0";
    
    char present_chain[2]; strcpy(present_chain , ">");
    char present_res_seq_plus_iCode[6]; strcpy(present_res_seq_plus_iCode , ">");
    
    inFile.open(params.dirpdbs + "/" + pdb_id + ".pdb");
    if (!inFile) {
        printf("This file: %s does not exist here, or is unreadable.\n", pdb_id.c_str());
        exit(EXIT_FAILURE);
    }
    
    // vector to store objects Chain.
    vector<Chain> v_chains = {};
    
    int idx_chain = -1;
    int idx_residue = -1;
        
    while (inFile) {
        inFile.getline(line, 85);

        if (inFile) {
            if (strncmp(line, "ATOM", 4) == 0) {
                // gets the chain.
                strncpy(chain, line+21, 1);
                
                // is a different chain?
                if (strcmp(chain, present_chain) != 0) {
                    
                    idx_chain++;
                    idx_residue = -1;
                    
                    // creates object Chain.
                    Chain new_chain;
                    new_chain.name = chain;
                    new_chain.protein = pdb_id;
                    new_chain.residues = {};
                    new_chain.ptree = kd_create(3);
                    
                    // adds the new chain.
                    v_chains.emplace_back(new_chain);

                    // updates current chain.
                    strcpy(present_chain, chain);
                    
                    // updates current residue.
                    strcpy(present_res_seq_plus_iCode , ">");
                } 
                
                // gets other values.
                sscanf(line+6, "%5d", &serial);
                sscanf(line+30, "%8f", &coord_x);
                sscanf(line+38, "%8f", &coord_y);
                sscanf(line+46, "%8f", &coord_z);

                strncpy(atom_name, line+12, 4);
                strncpy(res_name, line+17, 3);
                strncpy(res_seq_plus_iCode, line+22, 5);

                strncpy(atom_name+4, "\0", 1);
                strncpy(res_name+3, "\0", 1);
                strncpy(chain+1, "\0", 1);
                strncpy(res_seq_plus_iCode+5, "\0", 1);
                                     
                // creates object Atom.
                Atom atm;
                atm.serial = serial;
                atm.name = atom_name;
                atm.coord[0] = coord_x;
                atm.coord[1] = coord_y;
                atm.coord[2] = coord_z;
                    
                // is a different residue?
                if (strcmp(res_seq_plus_iCode, present_res_seq_plus_iCode) != 0) {
                    
                    idx_residue++;
                    
                    // creates object Residue.
                    Residue new_res;
                    new_res.name = trim(res_name);
                    new_res.res_id = atoi(res_seq_plus_iCode);
                    new_res.geom_center[0] = new_res.geom_center[1] = new_res.geom_center[2] = 0.0;
                    new_res.atoms = {};
                    
                    // adds new Residue to the Chain. 
                    v_chains[idx_chain].residues.emplace_back(new_res);
                    
                    // updates current residue.
                    strcpy(present_res_seq_plus_iCode, res_seq_plus_iCode);
                }

                // adds object ATOM to current Residue.
                v_chains[idx_chain].residues[idx_residue].atoms.emplace_back(atm);

            } else {
                // are there models?
                if ((strncmp(line, "ENDMDL", 6) == 0)) {
                    break;
                }
            }
        }
    }
    
    inFile.close();
    
    return v_chains;
}

/**/
void print_chains (vector<Chain> chains) {
    for (Chain chain: chains) {
        cout << "-------------------------" << endl;
        cout << chain.protein << " " << chain.name << ": " << endl;
        
        for (Site site: chain.sites) {
            cout << chain.protein << " " << chain.name << " " << site.pattern << " " << site.str_site << endl;
        }
    }
}

/**/
void calc_geom_center_of_residues(Chain *chain) {
    int n_residues = chain->residues.size();
    for (int r=0; r<n_residues; r++) {
        calc_geom_center_group_r(&chain->residues[r]);
    }
}

/**/
void calc_max_min_coordinates(Chain *chain) {
    float *gc;    
    bool first_step = true;
    
    for (Residue residue: chain->residues) {
        gc = residue.geom_center;
        
        if (first_step == true) {
            chain->xmin = gc[0]; chain->xmax = gc[0];
            chain->ymin = gc[1]; chain->ymax = gc[1];
            chain->zmin = gc[2]; chain->zmax = gc[2];
            first_step = false;
        } else {
            if (gc[0] <= chain->xmin)
                chain->xmin = gc[0];
            else if (gc[0] > chain->xmax) 
                chain->xmax = gc[0];
                
            if (gc[1] <= chain->ymin) 
                chain->ymin = gc[1];
            else if (gc[1] > chain->ymax) 
                chain->ymax = gc[1];
                
            if (gc[2] <= chain->zmin) 
                chain->zmin = gc[2];
            else if (gc[2] > chain->zmax) 
                chain->zmax = gc[2];
        }
    }
}

/**/
void find_sites(Chain *chain, parameters params) {
    unsigned int u_x, u_y, u_z;
    unsigned int maxu_x, maxu_y, maxu_z;
    
    maxu_x = (chain->xmax - chain->xmin)/params.step + 1;
    maxu_y = (chain->ymax - chain->ymin)/params.step + 1;
    maxu_z = (chain->zmax - chain->zmin)/params.step + 1;
    
    float x, y, z;
    string vcoord = "\0";
    
    // calculate extensions?
    if (params.extend > 0) {
        for (u_x=0; u_x<maxu_x; u_x++) {
            for (u_y=0; u_y<maxu_y; u_y++) {
                for (u_z=0; u_z<maxu_z; u_z++) {

                    x = chain->xmin + params.step * u_x;
                    y = chain->ymin + params.step * u_y;
                    z = chain->zmin + params.step * u_z;
                    
                    vector<Site> extend_sites;
                    list<int> neigh_list_central;
                    float point[3] = { x, y, z };
                    Site new_site;
                    float pleft1[3], pleft2[3], pright1[3], pright2[3];
                        
                    neigh_list_central = search_neighbors_array(chain, point, params.radius);

                    // X
                    pleft1[0]  = x+(params.extend);    pleft1[1] = y;  pleft1[2] = z;
                    pleft2[0]  = x+(params.extend*2);  pleft2[1] = y;  pleft2[2] = z;
                    pright1[0] = x-(params.extend);   pright1[1] = y; pright1[2] = z;
                    pright2[0] = x-(params.extend*2); pright2[1] = y; pright2[2] = z;
                    new_site = calculate_point_extensions(chain, neigh_list_central, point,
                        pleft1, pright1, pleft2, pright2, params);
                    if ((int)new_site.residues.size() >= params.nres)
                        extend_sites.emplace_back(new_site);
                    
                    // Y
                    pleft1[0]  = x; pleft1[1]  = y+(params.extend);    pleft1[2] = z;
                    pleft2[0]  = x; pleft2[1]  = y+(params.extend*2);  pleft2[2] = z;
                    pright1[0] = x; pright1[1] = y-(params.extend);   pright1[2] = z;
                    pright2[0] = x; pright2[1] = y-(params.extend*2); pright2[2] = z;
                    new_site = calculate_point_extensions(chain, neigh_list_central, point,
                        pleft1, pright1, pleft2, pright2, params);
                    if ((int)new_site.residues.size() >= params.nres)
                        extend_sites.emplace_back(new_site);

                    // Z
                    pleft1[0]  = x; pleft1[1]  = y;  pleft1[2] = z+(params.extend);
                    pleft2[0]  = x; pleft2[1]  = y;  pleft2[2] = z+(params.extend*2);
                    pright1[0] = x; pright1[1] = y; pright1[2] = z-(params.extend);
                    pright2[0] = x; pright2[1] = y; pright2[2] = z-(params.extend*2);
                    new_site = calculate_point_extensions(chain, neigh_list_central, point,
                        pleft1, pright1, pleft2, pright2, params);
                    if ((int)new_site.residues.size() >= params.nres)
                        extend_sites.emplace_back(new_site);
                            
                    // A-B
                    pleft1[0]  = x-(params.extend);   pleft1[1]  = y+(params.extend);    pleft1[2] = z+(params.extend);
                    pleft2[0]  = x-(params.extend*2); pleft2[1]  = y+(params.extend*2);  pleft2[2] = z+(params.extend*2);
                    pright1[0] = x+(params.extend);   pright1[1] = y-(params.extend);   pright1[2] = z-(params.extend);
                    pright2[0] = x+(params.extend*2); pright2[1] = y-(params.extend*2); pright2[2] = z-(params.extend*2);
                    new_site = calculate_point_extensions(chain, neigh_list_central, point,
                        pleft1, pright1, pleft2, pright2, params);
                    if ((int)new_site.residues.size() >= params.nres)
                        extend_sites.emplace_back(new_site);
                            
                    // E-F
                    pleft1[0]  = x-(params.extend);   pleft1[1]  = y+(params.extend);    pleft1[2] = z-(params.extend);
                    pleft2[0]  = x-(params.extend*2); pleft2[1]  = y+(params.extend*2);  pleft2[2] = z-(params.extend*2);
                    pright1[0] = x+(params.extend);   pright1[1] = y-(params.extend);   pright1[2] = z+(params.extend);
                    pright2[0] = x+(params.extend*2); pright2[1] = y-(params.extend*2); pright2[2] = z+(params.extend*2);
                    new_site = calculate_point_extensions(chain, neigh_list_central, point,
                        pleft1, pright1, pleft2, pright2, params);
                    if ((int)new_site.residues.size() >= params.nres)
                        extend_sites.emplace_back(new_site);
                            
                    // G-H
                    pleft1[0]  = x-(params.extend);   pleft1[1]  = y-(params.extend);    pleft1[2] = z-(params.extend);
                    pleft2[0]  = x-(params.extend*2); pleft2[1]  = y-(params.extend*2);  pleft2[2] = z-(params.extend*2);
                    pright1[0] = x+(params.extend);   pright1[1] = y+(params.extend);   pright1[2] = z+(params.extend);
                    pright2[0] = x+(params.extend*2); pright2[1] = y+(params.extend*2); pright2[2] = z+(params.extend*2);
                    new_site = calculate_point_extensions(chain, neigh_list_central, point,
                        pleft1, pright1, pleft2, pright2, params);
                    if ((int)new_site.residues.size() >= params.nres)
                        extend_sites.emplace_back(new_site);
                            
                    // C-D
                    pleft1[0]  = x-(params.extend);   pleft1[1]  = y-(params.extend);    pleft1[2] = z+(params.extend);
                    pleft2[0]  = x-(params.extend*2); pleft2[1]  = y-(params.extend*2);  pleft2[2] = z+(params.extend*2);
                    pright1[0] = x+(params.extend);   pright1[1] = y+(params.extend);   pright1[2] = z-(params.extend);
                    pright2[0] = x+(params.extend*2); pright2[1] = y+(params.extend*2); pright2[2] = z-(params.extend*2);
                    new_site = calculate_point_extensions(chain, neigh_list_central, point,
                        pleft1, pright1, pleft2, pright2, params);
                    if ((int)new_site.residues.size() >= params.nres)
                        extend_sites.emplace_back(new_site);
                    
                    for (long unsigned int i=0; i<extend_sites.size(); i++) {
                        // new site exist?
                        string str_site = extend_sites[i].str_site;
                        const auto site = std::find_if (
                            chain->sites.begin(),
                            chain->sites.end(),
                            [str_site](const Site &s) {
                                return s.str_site == str_site;
                            }
                        );
                        
                        if (site == chain->sites.end()) {
                            chain->sites.emplace_back(extend_sites[i]);
                        }
                    }
                }
            }
        }
    } else {
        for (u_x=0; u_x<maxu_x; u_x++) {
            for (u_y=0; u_y<maxu_y; u_y++) {
                for (u_z=0; u_z<maxu_z; u_z++) {
                    x = chain->xmin + params.step * u_x;
                    y = chain->ymin + params.step * u_y;
                    z = chain->zmin + params.step * u_z;
                    
                    // central point.
                    float point[3] = { x, y, z };
                    
                    list<int> neigh_list_central = search_neighbors_array(chain, point, params.radius);
                    
                    // site has minimun of residues?
                    if ((int) neigh_list_central.size() >= params.nres) {
                        
                        vcoord = to_string(point[0]) + "," + to_string(point[1]) + "," + to_string(point[2]);
                        Site new_site = create_site(chain, neigh_list_central, vcoord);
                        
                        if ((int)new_site.residues.size() >= params.nres) {
                            // new site exist?
                            string str_site = new_site.str_site;
                            const auto site = std::find_if (
                                chain->sites.begin(),
                                chain->sites.end(),
                                [str_site](const Site &s) {
                                    return s.str_site == str_site;
                                }
                            );
                            
                            if (site == chain->sites.end()) {
                                chain->sites.emplace_back(new_site);
                            }
                        }
                    }
                }
            }
        }
    }
}

/**/
void merge_patterns_in_chain(Chain *chain, parameters params) {
    int n_sites = chain->sites.size();
    string pattern_string = "\0";
    string protein_string = "\0";
    vector<Pattern>::iterator pattern_iter;
    vector<string>::iterator protein_iter;
    
    for (int s=0; s<n_sites; s++) {
        pattern_string = chain->sites[s].pattern;
            
        // find "pattern_string" chain->patterns.
        pattern_iter = std::find_if (
            chain->patterns.begin(),
            chain->patterns.end(),
            [pattern_string](const Pattern &p) {
                return p.name == pattern_string;
            }
        );
            
        // pattern_string exists?.
        if (pattern_iter == chain->patterns.end()) {
            // create a Pattern and relate the site to it.
            Pattern new_pattern;
            new_pattern.name = pattern_string;
            new_pattern.clusters = {};
            new_pattern.sites = {};
            new_pattern.sites.insert(new_pattern.sites.end(), chain->sites[s]);
            
            // protein where the site pattern appears.
            new_pattern.in_protein.emplace_back(chain->sites[s].protein);
                
            // adds the new pattern to vector.
            chain->patterns.emplace_back(new_pattern);
        } else {
            // pattern exist. relate the site to it.
            pattern_iter->sites.insert(pattern_iter->sites.end(), chain->sites[s]);
                
            // protein where the site pattern appears. 
            // The proteins could be repeated. 
            // Duplicates are removed later.
            pattern_iter->in_protein.emplace_back(chain->sites[s].protein);
        }
    }

    //
    /*
    for (Pattern pa: chain->patterns) {
        printf("%s %d\n",pa.name.c_str(), pa.sites.size());
        for (Site *si: pa.sites) {
            printf("\t%s\n", si->str_site.c_str());
        }
    }*/
}

/**/
void create_kdtree(Chain *chain) {
    int n_residues = chain->residues.size();
    
    for (int i=0; i<n_residues; i++) {
        vector<Atom> atoms = chain->residues[i].atoms;
        int n_atoms = atoms.size();
        
        for (int j=0; j<n_atoms; j++) {
            kd_insert(chain->ptree, atoms[j].coord, &chain->residues[i].res_id);
        }
    }
}

/**/
list<int> search_neighbors_array(Chain *chain, float *point, float radius) {
    // gets the neighbors (ids of residues).
    struct res_node v_res_node[1024];
    int n_elems = 0;
        
    kd_nearest_range_array(chain->ptree, point, radius, v_res_node, &n_elems);
    
    // converts results to a list. (ORDERED by id number and UNIQUEs)
    list<int> neigh_list = kdres_to_list_array(v_res_node, n_elems);
    
    return neigh_list;
}

/**/
Site calculate_point_extensions (Chain *chain, list<int> neigh_list_central, float *center,
    float *pleft1, float *pright1, float *pleft2, float *pright2, parameters params) {
    list<int> neigh_list;
    list<int> tmp0;
    list<int> tmp1;
    list<int> tmp2;
    list<int> tmp3;
    
    // inserts neighbors of the central point.
    neigh_list.insert(neigh_list.end(), neigh_list_central.begin(), neigh_list_central.end());
        
    tmp0 = search_neighbors_array(chain, pleft1, params.radius);
    tmp1 = search_neighbors_array(chain, pleft2, params.radius);
    tmp2 = search_neighbors_array(chain, pright1, params.radius);
    tmp3 = search_neighbors_array(chain, pright2, params.radius);

    neigh_list.insert(neigh_list.end(), tmp0.begin(), tmp0.end());
    neigh_list.insert(neigh_list.end(), tmp1.begin(), tmp1.end());
    neigh_list.insert(neigh_list.end(), tmp2.begin(), tmp2.end());
    neigh_list.insert(neigh_list.end(), tmp3.begin(), tmp3.end());
    
    // sort the total list.
    neigh_list.sort();
    // removes duplicated.
    neigh_list.unique(compare_int);
    
    //
    string vcoord = "\0";
    vcoord = vcoord + to_string(center[0]) + "," + to_string(center[1]) + "," + to_string(center[2]) + ":";
    vcoord = vcoord + to_string(pleft1[0]) + "," + to_string(pleft1[1]) + "," + to_string(pleft1[2]) + ":";
    vcoord = vcoord + to_string(pleft2[0]) + "," + to_string(pleft2[1]) + "," + to_string(pleft2[2]) + ":";
    vcoord = vcoord + to_string(pright1[0]) + "," + to_string(pright1[1]) + "," + to_string(pright1[2]) + ":";
    vcoord = vcoord + to_string(pright2[0]) + "," + to_string(pright2[1]) + "," + to_string(pright2[2]);

    Site new_site = create_site(chain, neigh_list, vcoord);
    
    return new_site;
}
