#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

class GeomCenter(object):
  def __init__(self, residue):
    self.residue = residue
    self.cg = []
    self.calcGeomCenter()

  def getGeomCenter (self):
    return self.cg

  '''
  calcula centro geométrico del grupo R de un residuo.
  '''
  def calcGeomCenter(self):
    atomlist = []
    
    if self.residue.get_resname() == 'GLY':
      # si no  tiene coordenadas...
      try:
        coor = self.residue['CA'].get_coord()
        self.cg = coor
      except:
        self.cg = None
    else:
      # obtiene lista con átomos para calcular el centro geométrico.
      for atom in self.residue:
        # filtra por átomos del grupo R
        if (atom.get_name()!='N' and atom.get_name()!='CA' and atom.get_name()!='C' 
          and atom.get_name()!='O' and atom.get_name()!='H'):
          atomlist.append(atom)

      # FIXED: debería encontrar al menos un átomo, sino ocurre, retorna la coordenada del
      if len(atomlist)==0:
        self.cg = self.residue['CA'].get_coord()
      else:
        # calcula centro geométrico.
        cg=np.sum(atom.get_coord() for atom in atomlist) / len(atomlist)
        self.cg = cg
