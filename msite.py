#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bisect
import collections

from geomcenter import GeomCenter
from pdbSite import PDBSite

class Site(object):
  def __init__(self, site, chain, pdbID, model, centerAtom, dirout):
    # site es una lista de Residue.
    self._site = site
    self._chain = chain
    self._pdbID = pdbID
    self._model = model
    self._centerAtom = str(centerAtom.get_parent().get_id()[1])
    self._dirout = dirout
    self._isValid = True
    self._pattern = ""
    self._resSeq = ""
    self._cluster = None

    self._resArrayCount = {'A':0, 'R':0, 'N':0, 'D':0, 'C':0, 'Q':0, 'E':0, 'G':0, 'H':0, 'I':0, 'L':0, 'K':0, 'M':0, 'F':0, 'P':0, 'S':0, 'T':0, 'W':0, 'Y':0, 'V':0}

    # diccionario
    self._resDict = {'ALA':'A', 'ARG':'R', 'ASN':'N', 'ASP':'D', 'CYS':'C', 'GLN':'Q', 'GLU':'E', 
  'GLY':'G', 'HIS':'H', 'ILE':'I', 'LEU':'L', 'LYS':'K', 'MET':'M', 'PHE':'F', 'PRO':'P', 
  'SER':'S', 'THR':'T', 'TRP':'W', 'TYR':'Y', 'VAL':'V'}
        
    # contabiliza las ocurrencias de los residuos.
    self._countResidues()
    
    # obtiene el patrón.
    self._createPattern()    
    
    if (self._isValid == True):
      # create PDB file of site.
      pdb = PDBSite(self._chain, self._dirout + "/pdbSite/" + self._resSeq + ".pdb", self.getResidues())

  def getCenterAtom(self):
    return self._centerAtom
  
  def setPatternCluster(self, cluster):
    self._cluster = cluster
  
  def getPatternCluster(self):
    return self._cluster
   
  def getResSeq (self):
    return self._resSeq
  
  def getPattern (self):
    return self._pattern
  
  def getIsValid (self):
    return self._isValid
  
  def getChainName (self):
    return self._chain
  
  def getResidues (self):
    lres = []
    for r in self._site:
      idRes = r.get_id()[1]
      res = self._model[self._chain][idRes]
      lres.append(res)
      
    return lres
  
  def _countResidues(self):
    # para concatenar secuencia de aminos del sitio.
    aminoSeq = []
      
    # por cada residuo del sitio.
    for residue in self._site:
      resname = ""
         
      # existe el Residuo en el diccionario?, es válido el nombre del residuo?
      if self._resDict.has_key(residue.get_resname()):

        # obtiene el código de una letra del residuo.
        resname = self._resDict[residue.get_resname()]
            
        # existe el código de una letra del residuo en arreglo que contabiliza apariciones?
        if self._resArrayCount.has_key(resname):
          # contabiliza aparación de residuo
          self._resArrayCount[resname] += 1
              
          # concatena residue (código 1 letra) con el ID de grupo del residuo.
          # agrega el residuo a la secuencia.
          ###aminoSeq.append(resname + str(residue.get_id()[1]))
          aminoSeq.append(residue.get_resname() + str(residue.get_id()[1]))
      else:
        # residuo no válido, un código de 3 letras no reconocido.
        ###print "Código 3 letras de residuo no identificado ..."
        ###return (False, None, None, None)
        self._isValid = False
        break
    
    # ordena la lista alfabéticamente.
    aminoSeq = sorted(aminoSeq)
    
    # modifica la secuencia del sitio para separarla ahora por ":"
    # agrega la proteína y la cadena al final de la secuencia separados por coma.
    # este valor será utilizado en la BD como atributo único identifique un SITE.
    self._resSeq = ":".join(aminoSeq) + "," + self._pdbID + "," + self._chain

  '''
    Genera el patrón de un sitio.
  '''
  def _createPattern(self):
    # ordena diccionario por key para mantener orden en nombre de patrones.
    self._resArrayCount = collections.OrderedDict(sorted(self._resArrayCount.items()))
    
    # por cada residuo en el diccionario.
    for res in self._resArrayCount:
      # el residuo tiene ocurrencias en la secuencia?
      if self._resArrayCount[res]>0:
        # genera el patrón concatenando el residuo y el número de ocurrencias.
        self._pattern = self._pattern + str(self._resArrayCount[res]) + res

