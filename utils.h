#include <set>

/* gets pdb ids from file.*/
vector<string> gets_pdbs_id_from_file(string pdb_list_file);
/**/
bool compare_int(int a, int b);
/**/
list<int> kdres_to_list_array(struct res_node *presults, int n_elems);
/**/
string trim(const string& str);
/**/
void print_results(vector<Pattern> v_patterns, int n_proteins);
/**/
void save_data_to_folder_version1(vector<Pattern> v_patterns, parameters params, vector<string> pdbs);
/**/
void save_data_to_folder_version2(std::set<Pattern> &set_patterns, parameters params, vector<string> pdbs);
/**/
string convert_vector_to_string(vector<string> vector);
/**/
void save_parameters(parameters params, vector<string> pdbs);
