#!/usr/bin/env python
#-*- coding: utf-8 -*-

import argparse
import ujson as json
import sparksee
import csv
from rmsd import RMSD

'''
  class.
'''
class DBGraphManager(object):
  def __init__(self, args, name):
    self.__args = args
    self.__name = name
    self.__cfg = None
    self.__db = None
    self.__sparks = None
    self.__dbfile = self.__args.dirout + "/dbgs/" + self.__name + ".db"
    self.__session = None
    self.__graph = None

  '''
    crea la base de datos principal
  '''
  def createMainDB(self):
    self.__cfg = sparksee.SparkseeConfig()
    self.__sparks = sparksee.Sparksee(self.__cfg)
    self.__db = self.__sparks.create(self.__dbfile, self.__name)
    self.__session = self.__db.new_session()
    self.__graph = self.__session.get_graph()
    self.__createMainSchema()
  
  """
    crea base de dato de sitios
  """
  def createSitesDB(self):
    self.__cfg = sparksee.SparkseeConfig()
    self.__sparks = sparksee.Sparksee(self.__cfg)
    self.__db = self.__sparks.create(self.__dbfile, self.__name)
    self.__session = self.__db.new_session()
    self.__graph = self.__session.get_graph()
    self.__createSitesSchema()

  """
  """
  def openSiteDB(self):
    self.__cfg = sparksee.SparkseeConfig()
    self.__sparks = sparksee.Sparksee(self.__cfg)
    # open on read mode.
    self.__db = self.__sparks.open(self.__dbfile, True)
    self.__session = self.__db.new_session()
    self.__graph = self.__session.get_graph()
    self.__getSiteTypes()

  '''
    cierra base de datos
  '''
  def closeDB(self):
    self.__session.close()
    self.__db.close()
    self.__sparks.close()

  '''
    crea el esquema de la base de datos
  '''
  def __createSitesSchema(self):
    # NODES
    self.__site_type_id = self.__graph.new_node_type(u"SITE")
    self.__site_attr_id = self.__graph.new_attribute(self.__site_type_id, u"ID", sparksee.DataType.STRING, sparksee.AttributeKind.UNIQUE)
    self.__site_attr_virtual_res = self.__graph.new_attribute(self.__site_type_id, u"VIRTUALRES", sparksee.DataType.STRING, sparksee.AttributeKind.BASIC)
    self.__site_attr_pattern = self.__graph.new_attribute(self.__site_type_id, u"PATTERN", sparksee.DataType.STRING, sparksee.AttributeKind.BASIC)
    self.__site_attr_chain = self.__graph.new_attribute(self.__site_type_id, u"CHAIN", sparksee.DataType.STRING, sparksee.AttributeKind.BASIC)
    self.__site_attr_rmsd = self.__graph.new_attribute(self.__site_type_id, u"RMSD", sparksee.DataType.DOUBLE, sparksee.AttributeKind.BASIC)

  '''
    obtiene los tipos y atributos.
  '''
  def __getSiteTypes(self):
    # NODES
    self.__site_type_id = self.__graph.find_type(u"SITE")
    self.__site_attr_id = self.__graph.find_attribute(self.__site_type_id, u"ID")
    self.__site_attr_virtual_res = self.__graph.find_attribute(self.__site_type_id, u"VIRTUALRES")
    self.__site_attr_pattern = self.__graph.find_attribute(self.__site_type_id, u"PATTERN")
    self.__site_attr_chain = self.__graph.find_attribute(self.__site_type_id, u"CHAIN")
    self.__site_attr_rmsd = self.__graph.find_attribute(self.__site_type_id, u"RMSD")

  """
  """    
  def __createMainSchema(self):
    # NODES
    self.__site_type_id = self.__graph.new_node_type(u"SITE")
    self.__site_attr_id = self.__graph.new_attribute(self.__site_type_id, u"ID", sparksee.DataType.STRING, sparksee.AttributeKind.UNIQUE)
    self.__site_attr_virtual_res = self.__graph.new_attribute(self.__site_type_id, u"VIRTUALRES", sparksee.DataType.STRING, sparksee.AttributeKind.BASIC)
    self.__site_attr_pattern = self.__graph.new_attribute(self.__site_type_id, u"PATTERN", sparksee.DataType.STRING, sparksee.AttributeKind.BASIC)
    self.__site_attr_chain = self.__graph.new_attribute(self.__site_type_id, u"CHAIN", sparksee.DataType.STRING, sparksee.AttributeKind.BASIC)
    self.__site_attr_rmsd = self.__graph.new_attribute(self.__site_type_id, u"RMSD", sparksee.DataType.DOUBLE, sparksee.AttributeKind.BASIC)

    self.__protein_type_id = self.__graph.new_node_type(u"PROTEIN")
    self.__protein_attr_id = self.__graph.new_attribute(self.__protein_type_id, u"ID", sparksee.DataType.STRING, sparksee.AttributeKind.UNIQUE)

    self.__pattern_type_id = self.__graph.new_node_type(u"PATTERN")
    self.__pattern_attr_id = self.__graph.new_attribute(self.__pattern_type_id, u"ID", sparksee.DataType.STRING, sparksee.AttributeKind.UNIQUE)
   
    self.__cluster_type_id = self.__graph.new_node_type(u"CLUSTER")
    self.__cluster_attr_id = self.__graph.new_attribute(self.__cluster_type_id, u"ID", sparksee.DataType.STRING, sparksee.AttributeKind.UNIQUE)

    # EDGES
    self.__site_in_cluster_type_id = self.__graph.new_edge_type(u"SITE_IN_CLUSTER", True, True)
    self.__site_base_in_cluster_type_id = self.__graph.new_edge_type(u"SITE_BASE_IN_CLUSTER", True, True)
    self.__cluster_in_pattern_type_id = self.__graph.new_edge_type(u"CLUSTER_IN_PATTERN", True, True)
    self.__pattern_in_protein_type_id = self.__graph.new_edge_type(u"PATTERN_IN_PROTEIN", True, True)
      
  '''
    crea un nodo con una proteína.
  '''
  def saveNodeProtein(self, proteinId):
    value = sparksee.Value()
    oidProtein = self.__graph.new_node(self.__protein_type_id)
    self.__graph.set_attribute(oidProtein, self.__protein_attr_id, value.set_string(proteinId))
    return oidProtein
  
  '''
    crea un nodo con un patrón.
  '''
  def saveNodePattern(self, pattern):
    value = sparksee.Value()
    oidPattern = self.__graph.new_node(self.__pattern_type_id)
    self.__graph.set_attribute(oidPattern, self.__pattern_attr_id, value.set_string(pattern))
    return oidPattern

  '''
    crea un nodo con un cluster.
  '''
  def saveNodeCluster(self, clusterName):
    value = sparksee.Value()
    oidCluster = self.__graph.new_node(self.__cluster_type_id)
    self.__graph.set_attribute(oidCluster, self.__cluster_attr_id, value.set_string(clusterName))
    return oidCluster

  '''
    verifica si ya existe un patrón.
  '''
  def existsNodePattern(self, pattern):
    value = sparksee.Value()
    Objs = self.__graph.select(self.__pattern_attr_id, sparksee.Condition.EQUAL, value.set_string(pattern))
    
    if Objs.count() > 0:
      oid = Objs.iterator().next()
      Objs.close()
      return oid
    else:
      Objs.close()
      return None
  
  """
  """
  def getProteins(self):
    data = []
    objs = self.__graph.select(self.__protein_type_id)
    for oid in objs:
      data.append(oid)
    objs.close()
    return data
  
  """
  """
  def getPatternInProtein(self, oidProtein):
    data = []
    objs = self.__graph.explode(oidProtein, self.__pattern_in_protein_type_id, sparksee.EdgesDirection.INGOING)
    for oid in objs:
      oidPattern = self.__graph.get_edge_data(oid).get_tail()
      data.append(oidPattern)
    objs.close()
    return data
  
  '''
    create node SITE.
  '''
  def saveNodeSite(self, site):
    value = sparksee.Value()
    oidSite = self.__graph.new_node(self.__site_type_id)
    self.__graph.set_attribute(oidSite, self.__site_attr_id, value.set_string(site.getResSeq()))
    self.__graph.set_attribute(oidSite, self.__site_attr_virtual_res, value.set_string(site.getCenterAtom()))
    self.__graph.set_attribute(oidSite, self.__site_attr_pattern, value.set_string(site.getPattern()))
    self.__graph.set_attribute(oidSite, self.__site_attr_chain, value.set_string(site.getChainName()))
    self.__graph.set_attribute(oidSite, self.__site_attr_rmsd, value.set_double(-1))
    return oidSite

  """
  """
  def saveNodeSiteNew(self, site):
    value = sparksee.Value()
    oidSite = self.__graph.new_node(self.__site_type_id)
    
    self.__graph.set_attribute(oidSite, self.__site_attr_id, value.set_string(site["id"]))
    self.__graph.set_attribute(oidSite, self.__site_attr_virtual_res, value.set_string(site["virtualres"]))
    self.__graph.set_attribute(oidSite, self.__site_attr_pattern, value.set_string(site["pattern"]))
    self.__graph.set_attribute(oidSite, self.__site_attr_chain, value.set_string(site["chain"]))
    self.__graph.set_attribute(oidSite, self.__site_attr_rmsd, value.set_double(site["rmsd"]))
    return oidSite
  
  '''
    verifica si ya existe un cluster.
  '''
  def existsNodeCluster(self, cluster):
    value = sparksee.Value()
    Objs = self.__graph.select(self.__cluster_attr_id, sparksee.Condition.EQUAL, value.set_string(cluster))
    
    if Objs.count() > 0:
      oid = Objs.iterator().next()
      Objs.close()
      return oid
    else:
      Objs.close()
      return None

  def existsEdgePatternInProtein(self, oidPattern, oidProtein):
    ###Graph.find_edge ( self, etype, tail, head)
    oidEdge = self.__graph.find_edge(self.__pattern_in_protein_type_id, oidPattern, oidProtein)
    
    # invalid oid?
    if oidEdge == 0:
      return None
    else:
      return oidEdge

  """
  """
  def saveEdgeSiteInCluster(self, oidSite, oidCluster):
    oidSiteInCluster = self.__graph.new_edge(self.__site_in_cluster_type_id, oidSite, oidCluster)
    return oidSiteInCluster

  """
  """  
  def saveEdgeSiteBaseInCluster(self, oidSite, oidCluster):
    oidSiteBaseInCluster = self.__graph.new_edge(self.__site_base_in_cluster_type_id, oidSite, oidCluster)
    return oidSiteBaseInCluster
    
  '''
    crea arista patrón en proteína.
  '''
  def saveEdgePatternInProtein(self, oidPattern, oidProtein):
    # new_edge (self, type, tail, head)
    oidPatternInProtein = self.__graph.new_edge(self.__pattern_in_protein_type_id, oidPattern, oidProtein)
    return oidPatternInProtein
      
  '''
    crea arista cluster in pattern.
  '''
  def saveEdgeClusterInPattern(self, oidCluster, oidPattern):
    # new_edge (self, type, tail, head)
    oidClusterInPattern = self.__graph.new_edge(self.__cluster_in_pattern_type_id, oidCluster, oidPattern)
    return oidClusterInPattern
  
  '''
    return statistics database.
  '''
  def getStatsDB(self):
    stats = sparksee.DatabaseStatistics()
    self.__db.get_statistics(stats)
    size = round(stats.get_data()/1024, 1)

    data = {'size': str(size) + "MB", 'cache': str(stats.get_cache()) + "KB", 'sessions': str(stats.get_sessions())}
    types = {}
    typeList = self.__graph.find_types()

    # recorre todos los tipos (nodos/aristas)
    for oidType in typeList:
      # el tipo de objeto
      tdata = self.__graph.get_type(oidType)
      # atributos del objeto
      aList = self.__graph.find_attributes(oidType)

      # lista con atributos del objeto
      attrList = ""

      # cada atributo del objeto
      for oidAttr in aList:
        # datos del atributo
        adata = self.__graph.get_attribute(oidAttr)
        # agrega a lista de atributos del objeto
        attrList = attrList + adata.get_name() + " "

      types[tdata.get_name()] = {'attributes': attrList, 'amount': str(tdata.get_num_objects())}

    jsonData = {}
    jsonData['stats'] = data
    jsonData['types'] = types

    with open(self.__args.dirout + "/json/dbstats.json", "w") as outfile:
      json.dump(jsonData, outfile)
    
    ###return jsonData
    
  '''
    Devuelve datos de la proteína.
  '''
  def getProteinData (self, oid):
    value = self.__graph.get_attribute(oid, self.__protein_attr_id)
    data = {"id": value.get_string()}
    return data

  """
  """
  def getSites(self):
    data = []
    objs = self.__graph.select(self.__site_type_id)
    for oid in objs:
      data.append(oid)
    objs.close()
    return data

  '''
    Genera detalles de cada proteina en archivos CSV.
  '''
  def getProteinsDetails(self):
    print ("Building CSV files with protein details")
    objProtein = self.__graph.select(self.__protein_type_id)
    
    # cada proteina.
    for oidProtein in objProtein:
      proteinData =  self.getProteinData(oidProtein)
      
      with open(self.__args.dirout + "/csv/" + proteinData["id"] + ".csv", 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', escapechar=' ', quoting=csv.QUOTE_NONE)
        lineTitle = ['pattern,cluster,site,chain,protein']
        writer.writerow(lineTitle)
        
        # patrones de la proteína
        oidsPattern = self.getPatternInProtein(oidProtein)
        for oidPattern in oidsPattern:
          patternData = self.getPatternData(oidPattern)
        
          # cluster del patrón.
          oidsCluster = self.getClusterInPattern(oidPattern)
          for oidCluster in oidsCluster:
            clusterData = self.getClusterData(oidCluster)
          
            # sitio base del cluster
            oidSiteBase = self.getSiteBaseInCluster(oidCluster)
            siteBaseData = self.getSiteData(oidSiteBase[0])
            siteBaseProtein = siteBaseData["id"].split(",")[1]
            if siteBaseProtein == proteinData["id"]:
              lineData = [patternData['id'], clusterData['id'], siteBaseData['id' ].split(",")[0], siteBaseData["chain"], siteBaseData["id"].split(",")[1]]
              writer.writerow(lineData)
            
            # sitios  del cluster
            oidsSites = self.getSiteInCluster(oidCluster)
            for oidSite in oidsSites:
              siteData = self.getSiteData(oidSite)
              siteProtein = siteData["id"].split(",")[1]
              
              if siteProtein == proteinData["id"]:
                lineData = [patternData['id'], clusterData['id'], siteData['id' ].split(",")[0], siteData["chain"], siteData["id"].split(",")[1]]
                writer.writerow(lineData)
            
        csvfile.close()
      
    objProtein.close()

  '''
    Obtiene los patrones desde la base de datos.
  '''
  def getPatterns(self):
    print ("Getting results ...")
    jsonData = {}
    allPattern = []
    allProtein = set()

    # obtiene cantidad de proteínas.
    Objs = self.__graph.select(self.__protein_type_id)
    nProt = Objs.count()    
    for oidProtein in Objs:
      allProtein.add(self.getProteinData(oidProtein)['id'])
    Objs.close()
    
    # obtiene todos los patrones
    objs = self.__graph.select(self.__pattern_type_id)
    for oidPattern in objs: 
      inProteins = set()
      patternData = self.getPatternData(oidPattern)

      # obtiene en cuantas proteinas está el patrón.
      ObjsEnProt = self.__graph.neighbors(oidPattern, self.__pattern_in_protein_type_id, sparksee.EdgesDirection.OUTGOING)
      enProt = ObjsEnProt.count()
      for o in ObjsEnProt:
        inProteins.add(self.getProteinData(o)['id'])
      ObjsEnProt.close()
      
      sIn = ','
      sIn = sIn.join(inProteins)
      
      notInProteins = allProtein - inProteins
      sNot = ","
      sNot = sNot.join(notInProteins)
      
      # calcula porcentaje de cobertura.
      percentagePattern = round((float(enProt)/float(nProt)*100), 1)
      
      #### ------------
      # imprime los cluster del patrón y obtiene la cantidad de cluster en el patrón.
      totalSites = 0
      allClusters = []

      # obtener la aristas.
      objsEdges = self.__graph.explode(oidPattern, self.__cluster_in_pattern_type_id, sparksee.EdgesDirection.INGOING)
      nCluster = objsEdges.count()
    
      # recorre cada arista (cada cluster) y obtiene sus sitios.
      for oidA in objsEdges:
        sitesInProtein = []
        allSites = []
      
        oidCluster = self.__graph.get_edge_data(oidA).get_tail()
        dataCluster = self.getClusterData(oidCluster)
        
        # obtiene sitio base del cluster.
        sitesBase = self.getSiteBaseInCluster(oidCluster)
        siteBaseData = self.getSiteData(sitesBase[0])
        lineSite = {"site": siteBaseData["id"].split(",")[0], 
          "protein": siteBaseData["id"].split(",")[1], "chain": siteBaseData["chain"], 
          "virtualres": siteBaseData["virtualres"], "rmsd": 0, "base": "yes"}
        allSites.append(lineSite)
        
        # obtiene los otros sitios del cluster.
        sites = self.getSiteInCluster(oidCluster)
        
        for oidSite in sites:
          siteData = self.getSiteData(oidSite)
          
          # prepara para JSON
          lineSite = {"site": siteData["id"].split(",")[0], "protein": siteData["id"].split(",")[1], 
            "chain": siteData["chain"], "virtualres": siteData["virtualres"], "rmsd": siteData["rmsd"], "base": "no"}
          
          #print lineSite
          
          allSites.append(lineSite) 
        
        # crea el archivo JSON
        with open(self.__args.dirout + "/json/sites-in-" +  dataCluster["id"] + ".json", "w") as outfileSites:
          json.dump(allSites, outfileSites)
        outfileSites.close()
        
        nSite = len(sitesBase) + len(sites)
        totalSites = totalSites + nSite
        
        # prepara para JSON
        lineCluster = {"cluster": dataCluster["id"], "nSite": nSite, "inProt": 1, "coverage": 0}
        
        # obtener en qué proteinas aparecen los sitios.
        for tmp in allSites:
          if tmp['protein'] not in sitesInProtein:
            sitesInProtein.append(tmp['protein'])
        
        # calcula porcentaje de cobertura.
        inProt = len(sitesInProtein)
        
        percentageCluster = round((float(inProt)/float(nProt)*100), 1)
        lineCluster['inProt'] = str(inProt)
        lineCluster['coverage'] = percentageCluster
          
        '''  
        # calcula el RMSD contra todos los sitios del cluster.
        # más de 1 un sitio asociado al cluster?, el base más los otros.
        if nSite>1:
          ###rms = RMSD(siteBaseData["id"], allSites, self.__args.dirOut + "/pdbSite/")
          rms = RMSD()
          rms.processListOfSites(siteBaseData["id"], allSites, self.__args.dirout + "/pdbSite/")
          lineCluster['media'] = round(float(rms.media), 1)
          lineCluster['varianza'] = round(float(rms.var), 1)
      
          percentageCluster = round((float(inProt)/float(nProt)*100), 1)
          ###lineCluster['inProt'] = str(inProt) + '/' + str(nProt)
          lineCluster['inProt'] = str(inProt)
          lineCluster['coverage'] = percentageCluster
        '''
        
        allClusters.append(lineCluster)
        
      objsEdges.close()
      
      # crea el archivo JSON
      with open(self.__args.dirout + "/json/clusters-in-" +  patternData["id"] + ".json", "w") as outfileCluster:
        json.dump(allClusters, outfileCluster)
      outfileCluster.close()

      #####----------------------------
      # Filtro para mostrar en los resultados.
      if percentagePattern >= self.__args.coverage:
        linePattern = {"pattern": patternData["id"], 
          "enProt": enProt, 
          "percentage": str(percentagePattern), 
          "nCluster": nCluster,
          "totalSitesCluster": totalSites,
          "notInProteins": sNot,
          "inProteins": sIn,
          "noEnProt": nProt - enProt}
        
        allPattern.append(linePattern)
      
    objs.close()

    # crea el archivo JSON
    with open(self.__args.dirout + "/json/patterns.json", "w") as outfilePattern:
      json.dump(allPattern, outfilePattern)
    outfilePattern.close()
  
  '''
    Listado de sitios en cluster.
  '''
  def getSiteInCluster(self, oidCluster):
    data = []
    objs = self.__graph.explode(oidCluster, self.__site_in_cluster_type_id, sparksee.EdgesDirection.INGOING)
    for oid in objs:
      oidSite = self.__graph.get_edge_data(oid).get_tail()
      data.append(oidSite)
    objs.close()
    return data

  """
  """
  def getSiteBaseInCluster(self, oidCluster):
    data = []
    objs = self.__graph.explode(oidCluster, self.__site_base_in_cluster_type_id, sparksee.EdgesDirection.INGOING)
    for oid in objs:
      oidSite = self.__graph.get_edge_data(oid).get_tail()
      data.append(oidSite)
    objs.close()
    return data

  """
  """
  def getClusterInPattern(self, oidPattern):
    data = []
    objs = self.__graph.explode(oidPattern, self.__cluster_in_pattern_type_id, sparksee.EdgesDirection.INGOING)
    for oid in objs:
      oidCluster = self.__graph.get_edge_data(oid).get_tail()
      data.append(oidCluster)
    objs.close()
    return data 
    
  '''
    devuelve diccionario con datos del patrón.
  '''
  def getPatternData (self, oid):
    value = self.__graph.get_attribute(oid, self.__pattern_attr_id)
    data = {"id": value.get_string()}
    return data
  
  '''
    devuelve diccionario con datos del sitio.
  '''
  def getSiteData (self, oid):
    value  = self.__graph.get_attribute(oid, self.__site_attr_id)
    value1 = self.__graph.get_attribute(oid, self.__site_attr_virtual_res)
    value2 = self.__graph.get_attribute(oid, self.__site_attr_pattern)
    value3 = self.__graph.get_attribute(oid, self.__site_attr_chain)
    value4 = self.__graph.get_attribute(oid, self.__site_attr_rmsd)
    
    data = {"id": value.get_string(), "virtualres": value1.get_string(), 
      "pattern": value2.get_string(), "chain": value3.get_string(),
      "rmsd": value4.get_double()
    }
    return data
  
  '''
    devuelve diccionario con datos del cluster.
  '''
  def getClusterData (self, oid):
    value = self.__graph.get_attribute(oid, self.__cluster_attr_id)
    data = {"id": value.get_string()}
    return data

  '''
    devuelve diccionario con datos de la arista site_in_cluster.
  '''
  def getSiteInClusterData (self, oid):
    value1 = self.__graph.get_attribute(oid, self.site_in_cluster_attr_protein)
    value2 = self.__graph.get_attribute(oid, self.site_in_cluster_attr_chain)
    data = {"protein": value1.get_string(), "chain": value2.get_string()}
    return data
 
