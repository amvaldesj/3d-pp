#include <map>
#include <vector>
#include <list>
#include <fstream>
#include <ctime>
#include <iomanip>      // std::setprecision
#include <chrono>
#include <iostream>     // std::fixed
using namespace std;

#include "kdtree/kdtree.h"
#include "structures.h"
#include "utils.h"

/**/
vector<string> gets_pdbs_id_from_file(string pdb_list_file) {
    ifstream inFile;
    char line[5];
    vector<string> pdbs;

    inFile.open(pdb_list_file);
    if (!inFile) {
        printf("This file: %s does not exist here, or is unreadable.\n", pdb_list_file.c_str());
        exit(EXIT_FAILURE);
    }

    while (inFile) {
        inFile.getline(line, 5);
        if (inFile) {
            pdbs.emplace_back(line);
        }
    }

    inFile.close();
    return pdbs;
}

/**/
bool compare_int(int a, int b) { 
    return ((a)==(b)); 
} 

/**/
list<int> kdres_to_list_array(struct res_node *presults, int n_elems) {
    list<int> lst;

    for (int i=0; i<n_elems; i++) {
        int *tmp = (int *)presults[i].item->data;
        if (*tmp != 0) {
            lst.emplace_back(*tmp);
        }
    }
    
    lst.sort();
    lst.unique(compare_int);
    
    return lst;
}

/**/
string trim(const string& str) {
    size_t first = str.find_first_not_of(' ');
    if (string::npos == first) {
        return str;
    }
    size_t last = str.find_last_not_of(' ');
    return str.substr(first, (last - first + 1));
}

/**/
void print_results(vector<Pattern> v_patterns, int n_proteins) {
    int n_patterns = v_patterns.size();

    for (int i=0; i<n_patterns; i++) {
        printf("Pattern: %s In proteins: %d Coverage: %.2f Clusters: %d\n", 
            v_patterns[i].name.c_str(), (int)v_patterns[i].in_protein.size(), 
            (float)v_patterns[i].in_protein.size()/n_proteins, 
            (int)v_patterns[i].clusters.size());
        
        for (Cluster cluster: v_patterns[i].clusters) {
            printf("\tCluster: %s In proteins: %d Coverage: %.2f Sites: %d\n",
                cluster.name.c_str(),
                (int)cluster.in_protein.size(),
                (float)cluster.in_protein.size()/n_proteins,
                (int)cluster.sites.size());
            
            for (Site site: cluster.sites) {
                printf("\t\t%s-%s-%s rmsd: %.2f\n",
                    site.str_site.c_str(),
                    site.chain.c_str(),
                    site.protein.c_str(),
                    (float)site.rmsd);
            }
        }
    }
}

/**/
void save_data_to_folder_version1(vector<Pattern> v_patterns, parameters params, vector<string> pdbs) {
    cout << "saving results... " << endl;
    auto start = chrono::system_clock::now();
    
    //
    string path = params.dirout;
    float cluster_coverage;
    int pattern_not_in;
    string pattern_in_protein_list = "\0";
    ofstream all_patterns_file;
    string pattern_file_path;
    ofstream pattern_file;
    string cluster_file_path;
    ofstream cluster_file;
   
    //
    string all_patterns_file_path  = params.dirout + "/patterns.json";
    all_patterns_file.open (all_patterns_file_path);
    bool first_pattern = true;
    all_patterns_file << "[";

    // explore all patterns.
    for (Pattern pattern: v_patterns) {
        //
        pattern_not_in = pdbs.size() - pattern.in_protein.size();
        pattern_in_protein_list = convert_vector_to_string(pattern.in_protein);

        bool first_cluster = true;
        float cluster_coverage_max = 0;

        //
        pattern_file_path  = params.dirout + "/" + pattern.name + ".json";
        pattern_file.open(pattern_file_path);
        pattern_file << "[";

        // saves cluster of this pattern.
        for (Cluster cluster: pattern.clusters) {
            bool first_site = true;
            cluster_coverage = ((float)cluster.in_protein.size()/(int)pattern.in_protein.size())*100;

            if (cluster_coverage > cluster_coverage_max)
                cluster_coverage_max = cluster_coverage;

            //
            if (first_cluster)
                first_cluster = false;
            else 
                pattern_file << ",\n";

            //
            pattern_file << "{\"cluster\": \"" << cluster.name << "\", ";
            pattern_file << "\"nsites\": " << cluster.sites.size() << ", ";
            pattern_file << "\"cluster_coverage\": " << fixed << setprecision(1) << cluster_coverage << ", ";
            pattern_file << "\"in_proteins\": " << cluster.in_protein.size() << "}";

            //
            cluster_file_path = params.dirout + "/" + cluster.name + ".json";
            cluster_file.open(cluster_file_path);
            cluster_file << "[";

            // saves sites of this cluster.
            int site_id = 1;
            for (Site site: cluster.sites) {
                //
                if (first_site)
                    first_site = false;
                else
                    cluster_file << ",\n";
                    
                cluster_file << "{\"site\": \"" << site.str_site << "\", ";
                cluster_file << "\"chain\": \"" << site.chain << "\", ";
                cluster_file << "\"protein\": \"" << site.protein << "\", ";
                cluster_file << "\"rmsd\": " << fixed << setprecision(2) << site.rmsd << ", ";
                cluster_file << "\"base\": " << site.base << ", ";
                cluster_file << "\"vcoord\": \"" << site.vcoord << "\", ";
                cluster_file << "\"id\": " << site_id;
                cluster_file << "}";

                site_id++;
            }

            //
            cluster_file << "]";
            cluster_file.close();
        }

        //
        pattern_file << "]";
        pattern_file.close();

        //
        if (first_pattern)
            first_pattern = false;
        else
            all_patterns_file << ",\n";

        //
        all_patterns_file << "{\"pattern\": \"" << pattern.name << "\", ";
        all_patterns_file << "\"nclusters\": " << pattern.clusters.size() << ", ";
        all_patterns_file << "\"pattern_coverage\": " << fixed << setprecision(1) << pattern.coverage << ", ";
        all_patterns_file << "\"pattern_in\": " << pattern.in_protein.size() << ", ";
        all_patterns_file << "\"cluster_coverage_max\": " << fixed << setprecision(1) << cluster_coverage_max << ", ";
        all_patterns_file << "\"pattern_not_in\": " << pattern_not_in << ", ";
        all_patterns_file << "\"pattern_in_protein_list\": \"" << pattern_in_protein_list << "\"} ";
    }

    //
    all_patterns_file << "]";
    all_patterns_file.close();
    
    //
    auto end = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = end-start;
    cout << elapsed_seconds.count() << " seconds" << endl;
}

void save_data_to_folder_version2(std::set<Pattern> &set_patterns, parameters params, vector<string> pdbs) {
    cout << "saving results... " << endl;
    auto start = chrono::system_clock::now();
    
    //
    string path = params.dirout;
    float cluster_coverage;
    int pattern_not_in;
    string pattern_in_protein_list = "\0";
    ofstream all_patterns_file;
    string pattern_file_path;
    ofstream pattern_file;
    string cluster_file_path;
    ofstream cluster_file;
   
    //
    string all_patterns_file_path  = params.dirout + "/patterns.json";
    all_patterns_file.open (all_patterns_file_path);
    bool first_pattern = true;
    all_patterns_file << "[";

    // explore all patterns.
    for (Pattern pattern: set_patterns) {
        //
        pattern_not_in = pdbs.size() - pattern.in_protein.size();
        pattern_in_protein_list = convert_vector_to_string(pattern.in_protein);

        bool first_cluster = true;
        float cluster_coverage_max = 0;

        //
        pattern_file_path  = params.dirout + "/" + pattern.name + ".json";
        pattern_file.open(pattern_file_path);
        pattern_file << "[";

        // saves cluster of this pattern.
        for (Cluster cluster: pattern.clusters) {
            bool first_site = true;
            cluster_coverage = ((float)cluster.in_protein.size()/(int)pattern.in_protein.size())*100;

            if (cluster_coverage > cluster_coverage_max)
                cluster_coverage_max = cluster_coverage;

            //
            if (first_cluster)
                first_cluster = false;
            else 
                pattern_file << ",\n";

            //
            pattern_file << "{\"cluster\": \"" << cluster.name << "\", ";
            pattern_file << "\"nsites\": " << cluster.sites.size() << ", ";
            pattern_file << "\"cluster_coverage\": " << fixed << setprecision(1) << cluster_coverage << ", ";
            pattern_file << "\"in_proteins\": " << cluster.in_protein.size() << "}";

            //
            cluster_file_path = params.dirout + "/" + cluster.name + ".json";
            cluster_file.open(cluster_file_path);
            cluster_file << "[";

            // saves sites of this cluster.
            int site_id = 1;
            for (Site site: cluster.sites) {
                //
                if (first_site)
                    first_site = false;
                else
                    cluster_file << ",\n";
                    
                cluster_file << "{\"site\": \"" << site.str_site << "\", ";
                cluster_file << "\"chain\": \"" << site.chain << "\", ";
                cluster_file << "\"protein\": \"" << site.protein << "\", ";
                cluster_file << "\"rmsd\": " << fixed << setprecision(2) << site.rmsd << ", ";
                cluster_file << "\"base\": " << site.base << ", ";
                cluster_file << "\"vcoord\": \"" << site.vcoord << "\", ";
                cluster_file << "\"id\": " << site_id;
                cluster_file << "}";

                site_id++;
            }

            //
            cluster_file << "]";
            cluster_file.close();
        }

        //
        pattern_file << "]";
        pattern_file.close();

        //
        if (first_pattern)
            first_pattern = false;
        else
            all_patterns_file << ",\n";

        //
        all_patterns_file << "{\"pattern\": \"" << pattern.name << "\", ";
        all_patterns_file << "\"nclusters\": " << pattern.clusters.size() << ", ";
        all_patterns_file << "\"pattern_coverage\": " << fixed << setprecision(1) << pattern.coverage << ", ";
        all_patterns_file << "\"pattern_in\": " << pattern.in_protein.size() << ", ";
        all_patterns_file << "\"cluster_coverage_max\": " << fixed << setprecision(1) << cluster_coverage_max << ", ";
        all_patterns_file << "\"pattern_not_in\": " << pattern_not_in << ", ";
        all_patterns_file << "\"pattern_in_protein_list\": \"" << pattern_in_protein_list << "\"} ";
    }

    //
    all_patterns_file << "]";
    all_patterns_file.close();
    
    //
    auto end = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = end-start;
    cout << elapsed_seconds.count() << " seconds" << endl;
}

/**/
string convert_vector_to_string(vector<string> vector) {
    string s = "\0";

    for (const auto &piece : vector) {
        if (s == "\0")
            s += piece;
        else
            s += ", " + piece;
    }
    return s;
}

/**/
void save_parameters(parameters params, vector<string> pdbs) {
    //
    ofstream params_file;
    string params_file_path  = params.dirout + "/params.json";

    time_t now = chrono::system_clock::to_time_t(chrono::system_clock::now());
    string job_date(20, '\0');
    std::strftime(&job_date[0], job_date.size(), "%Y-%m-%d %H:%M:%S", std::localtime(&now));

    string protein_list =  convert_vector_to_string(pdbs);

    //
    params_file.open (params_file_path);
    params_file << "{";
    params_file << "\"step\": " << fixed << setprecision(1) << params.step << ", ";
    params_file << "\"rmsd\": " << fixed << setprecision(1) << params.rmsd << ", ";
    params_file << "\"radius\": " << fixed << setprecision(1) << params.radius << ", ";
    params_file << "\"extend\": " << fixed << setprecision(1) << params.extend << ", ";
    params_file << "\"coverage\": " << fixed << setprecision(1) << params.coverage << ", ";
    params_file << "\"nproteins\": " << pdbs.size() << ", ";
    params_file << "\"date\": \"" << job_date.c_str() << "\", ";
    params_file << "\"proteins\": \"" << protein_list << "\" ";
    params_file << "}";

    params_file.close();
}
