prefix=/usr/local
CC = g++
kdlib = kdtree/libkdtree.a

#
#CFLAGS = -DVERSION1 -O3 -std=c++11 -fopenmp -march=native -pipe 
CFLAGS = -DVERSION2 -O3 -std=c++11 -fopenmp -march=native -pipe 
#CFLAGS = -DVERSION3 -O3 -std=c++11 -fopenmp -march=native -pipe 
LDFLAGS = $(kdlib) 

# Instrumentation framework -lomptrace (extrae)
##LDFLAGS = $(kdlib) /apps/BSCTOOLS/extrae/4.0.0/openmpi_3_1_0/lib/libomptrace.so
#LDFLAGS = $(kdlib) -lomptrace 

#CFLAGS = -DTRACING -DVERSION1 -O3 -std=c++11 -fopenmp -march=native -pipe 
#CFLAGS = -DTRACING -DVERSION2 -O3 -std=c++11 -fopenmp -march=native -pipe 
#CFLAGS = -DTRACING -DVERSION3 -O3 -std=c++11 -fopenmp -march=native -pipe 

SRC = 3d-pp.cpp chain.cpp residue.cpp atom.cpp site.cpp pattern.cpp rmsd.cpp utils.cpp
OBJ = 3d-pp.o chain.o residue.o atom.o site.o pattern.o rmsd.o utils.o
APP = 3d-pp

all: $(OBJ)
	$(CC) $(CFLAGS) -o $(APP) $(OBJ) $(LIBS) $(LDFLAGS)

%.o: %.cpp
	$(CC) $(CFLAGS) -c $<

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
