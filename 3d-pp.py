#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import ujson as json
from subprocess import call
import csv
import multiprocessing
import datetime
import numpy as np
import operator

from parsePDB import ParsePDB
from dbGraphManager import DBGraphManager
from rmsd import RMSD

class MainApp(object):
  def __init__(self):
    # list with IDs of protein processed.
    self.__proteins = []
    self.__proteins_not_process = []
    self.__proteins_repeated = []
    self.__args = None
    self.__pool = multiprocessing.Pool(processes=multiprocessing.cpu_count())
    self.__lParse = []
    self.__date =  datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
    self.__mainDB = None
    
    self.__format = '%H:%M:%S'
    self.__begin = datetime.datetime.strftime(datetime.datetime.now(), self.__format)
    
    # read options from terminal.
    self.__readOptions()

    # create folders.
    self.__createFolder()
    
    # Parse the proteins.
    self.__parsePDBs()
    
    # Discover patterns. Process all graph databases.
    self.__mainDB = DBGraphManager(self.__args, "mainDB")
    self.__mainDB.createMainDB()
    
    # unifica sitios de cada proteina procesada en BD principal
    self.__discoverPatterns()

    # obtener resultados.
    self.__mainDB.getPatterns()
    
    self.__mainDB.getStatsDB()
    self.__mainDB.closeDB()

    self.__end = datetime.datetime.strftime(datetime.datetime.now(), self.__format)    
    self.__time = str(datetime.datetime.strptime(self.__end, self.__format) - datetime.datetime.strptime(self.__begin, self.__format))
    
    # Generar archivos JSON.
    self.__saveParams()
    
    
    print "time (H:M:S):", self.__time
    
    self.__deleteFolder()
  
  """
  """
  def __discoverPatterns(self):    
    for protein in self.__proteins:
      print ("protein " + protein + ": unifiying!")
      
      # create node PROTEIN
      oidProtein = self.__mainDB.saveNodeProtein(protein)
      
      # temporal graph database (for each protein processed.)
      tmpDB = DBGraphManager(self.__args, protein)
      tmpDB.openSiteDB()
      
      # get sites found in the protein.
      sites = tmpDB.getSites()
      
      # for each sites in the protein.
      for oidSite in sites:
        siteData =  tmpDB.getSiteData(oidSite)
        
        # create node SITE
        ###oidSiteNew = self.__mainDB.saveNodeSiteNew(siteData)
        
        # verify if pattern already exists in the database. 
        oidPattern = self.__mainDB.existsNodePattern(siteData["pattern"])
        
        # pattern NOT exists!
        if oidPattern == None:
          # create node PATTERN
          oidPattern = self.__mainDB.saveNodePattern(siteData["pattern"])
          
          # create edge PATTERN_IN_PROTEIN
          oidPatternInProtein = self.__mainDB.saveEdgePatternInProtein(oidPattern, oidProtein)

          # create node CLUSTER
          clusterInProtein = self.__mainDB.getClusterInPattern(oidPattern)
          clusterName = siteData["pattern"] + "-" + str(len(clusterInProtein) + 1)
          oidCluster = self.__mainDB.saveNodeCluster(clusterName)

          # create edge CLUSTER_IN_PATTERN
          oidEdgeClusterInPattern = self.__mainDB.saveEdgeClusterInPattern(oidCluster, oidPattern)          

          # create node SITE
          oidSiteNew = self.__mainDB.saveNodeSiteNew(siteData)
        
          # create edge SITE_BASE_IN_CLUSTER
          oidSiteBaseInCluster = self.__mainDB.saveEdgeSiteBaseInCluster(oidSiteNew, oidCluster)
          
        # pattern exists!
        else:
          # verificate if already exists edge PATTERN_IN_PROTEIN
          oidPatternInProtein = self.__mainDB.existsEdgePatternInProtein(oidPattern, oidProtein)
          
          if oidPatternInProtein == None:
            # create edge PATTERN_IN_PROTEIN
            oidPatternInProtein = self.__mainDB.saveEdgePatternInProtein(oidPattern, oidProtein)

          # match the site in a cluster, considering rmsd.
          (oidCluster, siteDataWithRMSD) = self.__matchCluster(oidPattern, siteData)
          
  
          # NO match!
          if oidCluster == None:
            # create node CLUSTER
            clusterInProtein = self.__mainDB.getClusterInPattern(oidPattern)
            clusterName = siteData["pattern"] + "-" + str(len(clusterInProtein) + 1)
            oidCluster = self.__mainDB.saveNodeCluster(clusterName)

            # create edge CLUSTER_IN_PATTERN
            oidEdgeClusterInPattern = self.__mainDB.saveEdgeClusterInPattern(oidCluster, oidPattern)          
            
            # create node SITE
            oidSiteNew = self.__mainDB.saveNodeSiteNew(siteData)
            
            # create edge SITE_BASE_IN_CLUSTER
            oidSiteBaseInCluster = self.__mainDB.saveEdgeSiteBaseInCluster(oidSiteNew, oidCluster)
          
          # fit in cluster!
          else:
            # create node SITE
            oidSiteNew = self.__mainDB.saveNodeSiteNew(siteDataWithRMSD)
            
            # create edge SITE_IN_CLUSTER
            oidSiteInCluster = self.__mainDB.saveEdgeSiteInCluster(oidSiteNew, oidCluster)

      tmpDB.closeDB()    
    
  """
  """
  def __matchCluster(self, oidPattern, siteData):
    
    # recorre cada uno de los clusters del patrón,
    # get CLUSTER_IN_PATTERN
    clusters = self.__mainDB.getClusterInPattern(oidPattern)
    for oidCluster in clusters:

      oidSiteBase = self.__mainDB.getSiteBaseInCluster(oidCluster)[0]
      siteBaseData = self.__mainDB.getSiteData(oidSiteBase)
      
      rmsdTmp = RMSD()
      rmsd = rmsdTmp.align(self.__args.dirout + "/pdbSite/" + siteBaseData["id"] + ".pdb", self.__args.dirout + "/pdbSite/" + siteData["id"] + ".pdb")
      rmsd = round(float(rmsd), 1)
      
      # evalua si cumple con la diferencia máxima del rmsd.
      #FIXME: EN EL PRIMERO QUE ENCAJA, PODRÏA ENCAJAR MEJOR QUIAS EN OTRO CLUSTER!!!
      if rmsd <= self.__args.diffrmsd:
        siteData["rmsd"] = rmsd
        
        # retorna el cluster en cual encaja.
        return oidCluster, siteData


    # no encaja en ningún cluster
    return None, None
    
  """
  """
  def __convertStringValuesToFloat(self, value):
    l = value.split(":")
    f = [float(n) for n in l]
    return f
    
  """
  """
  def __parsePDBs (self):
    # get all IDs from the file.
    fileIds = self.__getPDBsID()
    
    for pdbID in fileIds:
      # convert to lowercase the IDs.
      pdbID = pdbID.rstrip('\n').lower().strip()
      
      # descarta línea vacia.
      if (len(pdbID) > 0):
        if (pdbID not in self.__proteins):
          try:
            test = open(self.__args.dirpdb + pdbID + ".pdb")
            
            # parse the protein. 
            self.__pool.apply_async(newProccess, [pdbID, self.__args],)
            print ("protein " + pdbID + ": processing!")
            
            '''
            pdbfile = self.__args.dirpdb + pdbID + ".pdb"
            parse = ParsePDB(pdbID, pdbfile, self.__args)
            tmpDB = DBGraphManager(self.__args, pdbID)
            tmpDB.createSitesDB()
            for c in parse.getChains():
              saveDataInDB(pdbID, c.getChainName(), c.getListSites(), tmpDB)
            tmpDB.closeDB()
            '''
            
            # append the protein to the list.
            self.__proteins.append(pdbID)
                
          except:
            self.__proteins_not_process.append(pdbID)
            print ("protein " + pdbID + ": not proccess!")
          
        else:
          self.__proteins_repeated.append(pdbID)
          print ("protein " + pdbID + ": already processed!")
    
    self.__pool.close()
    self.__pool.join()
  
  """
  """
  def __createFolder(self):
    call(["mkdir", self.__args.dirout + "/json"])
    call(["mkdir", self.__args.dirout + "/pdbSite"])
    call(["mkdir", self.__args.dirout + "/dbgs"])

  """
  """
  def __deleteFolder(self):
    call(["rm", "-r", "-f", self.__args.dirout + "/dbgs/"])
    
  """
    returns File object with lists of PDB.
  """
  def __getPDBsID (self):
    fileIds = open(self.__args.fileid, 'r')
    return fileIds
    
  """
  """
  def __saveParams (self):
    jsonParams = {'radius': self.__args.radius, 
                  'step': self.__args.step, 
                  'proteins': self.__proteins, 
                  'coverage': self.__args.coverage, 
                  'diffrmsd': self.__args.diffrmsd, 
                  #'percSim': self.__args.percSim,
                  'notprocess': self.__proteins_not_process,
                  'repeated': self.__proteins_repeated,
                  'date': self.__date,
                  'time': self.__time}
                  
    with open(self.__args.dirout + "/json/params.json", "w") as outfile:
      json.dump(jsonParams, outfile)
  
  """
  """
  def __readOptions (self):
    parser = argparse.ArgumentParser(description='Discovering 3D patterns in proteins.')
    
    #parser.add_argument('listPDBs', metavar='listPDBs', help='A text file with the protein IDs.')
    parser.add_argument('-fileid', action='store', metavar='fileid', dest='fileid', help='A text file with the protein IDs.')
    
    #parser.add_argument('dirOut', metavar='dirOut', help='Folder for the results.')
    parser.add_argument('-dirout', action='store', metavar='dirout', dest='dirout', help='Folder for the results.')
    
    #parser.add_argument('step', metavar='step', type=float, help='The distance to create Grid of Virtual Coordinates (GvC).')
    parser.add_argument('-step', action='store', metavar='step', dest='step', type=float, help='The distance to create Grid of Virtual Coordinates (GvC).')
    
    #parser.add_argument('thresholdMax', metavar='thresholdMax', type=float, help='The maximum radius threshold (in Angstrom) for sites search, from the Virtual Coordinates (VC).')
    parser.add_argument('-radius', action='store', metavar='radius', dest='radius', type=float, help='The maximum radius threshold (in Angstrom) for sites search, from the Virtual Coordinates (VC).')
    
    #parser.add_argument('dirPDB', metavar='dirPDB', help='Folder with PDB files.')
    parser.add_argument('-dirpdb', action='store', metavar='dirpdb', dest='dirpdb', help='Folder with PDB files.')
    
    parser.add_argument('-diffrmsd', action='store', metavar='diffrmsd', dest='diffrmsd', type=float, help='The maximum difference (in Angstrom) between the RMSDs of the sites (for clustering)')
    
    #parser.add_argument('coverage', metavar='coverage', type=float, help='Minimum coverage percentage (results in web site).')
    parser.add_argument('-coverage', action='store', metavar='coverage', dest='coverage', type=float, help='Minimum percentage of coverage (for show results in web site).')
    
    #parser.add_argument('percSim', metavar='percSim', type=float, help='The minimum percentage of angles in a site that meet the maximum degrees difference.')
    #parser.add_argument('-diffrmsd', action='store', metavar='diffrmsd', dest='diffrmsd', type=float, help='The minimum percentage of angles in a site that meet the maximum degrees difference.')
    
    self.__args = parser.parse_args()
      
"""
"""
def saveDataInDB(pdbID, chain, sites, tmpDB):
  for site in sites:
    if site.getIsValid() == True:       
      # create node SITE.
      oidSite = tmpDB.saveNodeSite(site)
    else:
      # invalid site.
      pass

"""
"""    
def newProccess (pdbID, args):
  print ("protein " + pdbID + ": proccessing!")
  pdbfile = args.dirpdb + pdbID + ".pdb"
  parse = ParsePDB(pdbID, pdbfile, args)
  
  tmpDB = DBGraphManager(args, pdbID)
  tmpDB.createSitesDB()

  for c in parse.getChains():
    saveDataInDB(pdbID, c.getChainName(), c.getListSites(), tmpDB)
      
  tmpDB.closeDB()
  
  print ("protein " + pdbID + ": end!")
  
  return

'''
  main function.
'''
def main():
  app = MainApp()

"""
"""
if __name__ == '__main__':
  main()
