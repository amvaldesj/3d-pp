import sys

# import 3D-PP module.
import dddpp

# reads arguments.
args = sys.argv
args = [x.encode() for x in args]

# run 3d-pp.
dddpp.main(args)
