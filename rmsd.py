#!/usr/bin/env python
# -*- coding: utf-8 -*-
import Bio.PDB
import numpy as np

from geomcenter import GeomCenter
from Bio.PDB.Atom import Atom
from numpy import array

class RMSD(object):
  def __init__(self):
    self.var = 0.0
    self.media = 0.0

  def processListOfSites(self, ref, lsites, path):
    rms = []
    
    for other in lsites:
      tmp = other['site'] + "," + other['protein'] + "," + other['chain']
      
      if ref != tmp:
        rms.append(self.align(path + ref + ".pdb", path + tmp + ".pdb"))

        # calculo de la media aritmetica.
        self.media = np.mean(rms)
    
        # calculo de de la varianza.
        self.var =  np.var(rms) 

  def align(self, ref, sample):
    rms = 0.0
    
    # Start the parser
    pdb_parser = Bio.PDB.PDBParser(QUIET = True)
    ###pdb_parser = Bio.PDB.PDBParser(PERMISSIVE=1)

    # Get the structures
    ref_structure = pdb_parser.get_structure("reference", ref)
    sample_structure = pdb_parser.get_structure("sample", sample)
    
    # Use the first model in the pdb-files for alignment
    # Change the number 0 if you want to align to another structure
    ref_model    = ref_structure[0]
    sample_model = sample_structure[0]

    # Make a list of the atoms (in the structures) you wish to align.
    # In this case we use CA atoms whose index is in the specified range
    ref_atoms = self.getAtoms(ref_model)
    sample_atoms = self.getAtoms(sample_model)    

    # Now we initiate the superimposer:
    super_imposer = Bio.PDB.Superimposer()
    super_imposer.set_atoms(ref_atoms, sample_atoms)
    super_imposer.apply(sample_model.get_atoms())

    # Print RMSD:
    rms = super_imposer.rms
   
    # Save the aligned version of 1UBQ.pdb
    self.saveAlignedPDB(sample_structure, sample)

    return rms
    
  def saveAlignedPDB(self, sample_structure, sample):
    io = Bio.PDB.PDBIO()
    io.set_structure(sample_structure) 
    io.save(sample + "-aligned.pdb")

  def getAtoms (self, model):
    atoms = []

    for ref_chain in model:
      # Iterate of all residues in each model in order to find proper atoms
      for ref_res in ref_chain:
        
        cg = GeomCenter(ref_res)
        cgCoor = cg.getGeomCenter()
        cgAtom = Atom('XX', array(cgCoor), 0, 1, ' ', ' XX', 0, 'X')
        
        atoms.append(ref_res['CA'])

        # the residue has a certain Atom?
        if ref_res.has_id("C"):
          atoms.append(ref_res['C'])
        else:
          #FIXME: Set correct atom
          atoms.append(ref_res['CA'])
        
        if ref_res.has_id("N"):
          atoms.append(ref_res['N'])
        else:
          #FIXME: Set correct atom
          atoms.append(ref_res['CA'])

        atoms.append(cgAtom)
        
    return atoms
  
