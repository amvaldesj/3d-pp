<?php
/*
 * uploadFile.php
 * 
 * Copyright 2015 Alejandro Valdés Jimenez <avaldes@utalca.cl>
 * 
 */

require_once 'lib/common.php';

$type = $_POST['type'];

# carpeta para resultados.
$path =  "results/" . microtime(true);
exec("mkdir " . $path);

switch ($type) {
  case "xray":
    $uploadFile = $path . "/lista.txt";
    
    if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadFile)) {
      # ignora saltos de linea y lineas vacias.
      $arrayList = file($uploadFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
  
      # convierte todo a minúscula.
      $arrayList = array_map('strtolower', $arrayList);
  
      $missing = array();
      foreach ($arrayList as $id) {
        $id = trim($id);
        if (strlen($id) > 0) {        
          if (!file_exists ($GLOBALS['PDBDIR'] . $id . ".pdb")) {
            array_push($missing, $id);
          }
        }
      }
  
      if (count($missing)>0) {
        echo json_encode(array("status"=>"missing", "msg"=>$missing));
    
      } else {
        echo json_encode(array("status"=>"ok", "path"=>$path));
      }
  
    } else {
      echo json_encode(array("status"=>"error", "msg"=>"error uploading file!"));
    }
    break;
    
  case "insilico":
    $uploadFile = $path . "/lista.zip";
    
    if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadFile)) {
      echo json_encode(array("status"=>"ok", "path"=>$path));
      
    } else {
      echo json_encode(array("status"=>"error", "msg"=>"error uploading file!"));
    }
    break;
}

?>
