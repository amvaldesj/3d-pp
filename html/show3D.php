<?php
require_once 'lib/common.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?php echo $GLOBALS['Version']; ?></title>

  <!-- ############### CSS ############## -->
  <link href="components/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>

<body>
  <?php echo navbar(); ?>
  
  <!-- /.container -->
  <div class="container">

    <!-- row -->
    <div class="row">
      <div class="col-md-2">
      </div>
      <!-- column -->
      <div class="col-md-8">
        <div class="panel panel-default">
          <div class="panel-heading">
            <?php
            $btn = <<<EOT
            <button onClick="downloadPDB('{$_GET['protein']}','{$_GET['chain']}','{$_GET['site']}');" type="button" class="btn btn-default btn-xs" title="Download PDB files (protein, ghost atoms, geometrical centers, site)">
              <i class="glyphicon glyphicon-download-alt"></i>
            </button>
            <button onClick="javascript:Jmol.script(jmolApplet1,'image')" type="button" class="btn btn-default btn-xs" title="Capture image">
              <i class="glyphicon glyphicon-camera"></i>
            </button>
EOT;
            echo $btn;
            ?>
            
            <span class="panel-title" id="panel-title">Protein</span>
          </div>
          
          <div class="panel-body" align="center">
            <div align="center" id="mydiv1"></div>
          </div>
          
          <div class="panel-footer">
            NOTE: Use your mouse to drag, rotate, and zoom in and out of the structure. <a href="http://wiki.jmol.org/index.php/Mouse_Manual" target="_blank">Help</a>
          </div>
        </div>
      </div>
    </div>

    <?php
      footer();
    ?>
  <!-- #container -->
  </div>
  
  <!-- Componentes -->
  <!-- ############### JS ############## -->  
  <script type="text/javascript" src="jmol/jsmol/JSmol.min.js"></script>
  <script type="text/javascript" src="js/settings.js"></script>
  <script type="text/javascript" src="js/3dview.js"></script>
  
  <script type="text/javascript">
   showSite('<?php echo $_GET["protein"]; ?>', '<?php echo $_GET["chain"]; ?>', '<?php echo $_GET["site"]; ?>');
  </script>
  
</body>
</html>


