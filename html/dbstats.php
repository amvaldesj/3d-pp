<?php
require_once 'lib/common.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?php echo $GLOBALS['Version']; ?></title>

  <!-- ############### CSS ############## -->
  <link href="components/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>

<body>
  <?php echo navbar(); ?>
  
  <!-- /.container -->
  <div class="container">

    <!-- row -->
    <div class="row">
      <div class="col-md-1">
      </div>
      <!-- column -->
      <div class="col-md-10">
        <div class="panel panel-default">
          <div class="panel-heading">            
            <span class="panel-title" id="panel-title">Graph DB Stats</span>
          </div>
          
          <div class="panel-body">
            <label>Size: &nbsp;</label><label id="size"></label><br>
            <label>Nodes/Edges:</label><br>
            <label id="lists"></label>
          </div>
          
          <div class="panel-footer">
          </div>
        </div>
      </div>
    </div>

    <?php
      footer();
    ?>
  <!-- #container -->
  </div>
  
  <!-- Componentes -->
  <!-- ############### JS ############## -->  
  <script src="components/jquery/jquery-2.1.4.min.js"></script>
  <script type="text/javascript" src="js/dbstats.js"></script>
  
  <script type="text/javascript">
    getStats();
  </script>
  
</body>
</html>


