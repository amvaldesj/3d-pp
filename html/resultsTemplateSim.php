<?php
require_once 'lib/common.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?php echo $GLOBALS['Version']; ?></title>
  
  <!-- ############### CSS ############## -->
  <link href="components/dataTables/jquery.dataTables.css" rel="stylesheet">
  <link href="components/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  
  <!-- ############### JS ############## -->
  <script src="components/jquery/jquery-2.1.4.min.js"></script>
  <script src="components/bootstrap/js/bootstrap.min.js"></script>
  <script src="components/dataTables/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="js/results.js"></script>
</head>

<body>
  <?php echo navbar(); ?>

  <!-- /.container -->
  <!--<div class="container">-->
    
    <!-- Parameters -->
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <span ><a data-toggle="collapse" href="#collapse1">View parameters</a></span>
          <span> | </span>
          <span><a href="index.php">View patterns by identity</a></span>
        </div>

        <div id="collapse1" class="panel-collapse collapse on">
          <div class="panel-body">
            <?php showParams(); ?>
          </div>
        </div>
      </div>
    </div>

    <!-- Patterns by similarity according to reactive group or R group -->
    <div class="col-md-12">
      
      <div class="panel panel-default">
        <div class="panel-heading">
          <span><a data-toggle="collapse" href="#collapse3">Patterns by similarity according to reactive group or R group</a></span>
        </div>

        <div id="collapse3" class="panel-collapse collapse in">
          <div class="panel-body">
            
            <div class="col-md-6">
              <div class="panel panel-default">
                
                <div class="panel-heading">
                  <div style="text-align:center">
                    <small>
                    Aliphatic <b>[A]</b>
                    Aromatic <b>[B]</b>
                    OH <b>[C]</b>
                    Acidic <b>[D]</b>
                    Acid amide <b>[E]</b>
                    Basic <b>[F]</b>
                    Sulphur <b>[G]</b>
                    Cyclic <b>[H]</b>
                    </small>
                  </div>
                </div>
                
                <div class="panel-body">
                  <div>
                    <table id="table-group1" class="table table-striped display compact"></table>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <span id="titlePatterns1">Identity patterns</span>
                  <span id="titlePatgroup1"></span>
                </div>
                <div class="panel-body">
                  <div>
                    <table id="table-patterns-group1" class="table table-striped display compact"></table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Patterns by similarity dependent in the property of R group -->
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <span><a data-toggle="collapse" href="#collapse4">Patterns by similarity dependent in the property of R group</a></span>
        </div>

        <div id="collapse4" class="panel-collapse collapse in">
          <div class="panel-body">

            <div class="col-md-6">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <div style="text-align:center">
                    <small>
                    Non polar <b>[A]</b>
                    Polar uncharged <b>[B]</b>
                    Polar charged <b>[C]</b>
                    Positively charged <b>[D]</b>
                    Negatively charged <b>[E]</b>
                    </small>
                  </div>
                </div>
                <div class="panel-body">
                  
                  <div>
                    <table id="table-group2" class="table table-striped display compact"></table>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <span id="titlePatterns2">Identity patterns</span>
                  <span id="titlePatgroup2"></span>
                </div>
                <div class="panel-body">
                  <div>
                    <table id="table-patterns-group2" class="table table-striped display compact"></table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Patterns by similarity dependent according to polarity of R group -->
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <span><a data-toggle="collapse" href="#collapse5">Patterns by similarity dependent according to polarity of R group</a></span>
        </div>

        <div id="collapse5" class="panel-collapse collapse in">
          <div class="panel-body">

            <div class="col-md-6">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <div style="text-align:center">
                    <small>
                    Non polar <b>[A]</b>
                    Flexible <b>[B]</b>
                    Polar <b>[C]</b>
                    </small>
                  </div>
                </div>
                <div class="panel-body">
                  <div>
                    <table id="table-group3" class="table table-striped display compact"></table>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <span id="titlePatterns3">Identity patterns</span>
                  <span id="titlePatgroup3"></span>
                </div>

                <div class="panel-body">
                  <div>
                    <table id="table-patterns-group3" class="table table-striped display compact"></table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  <!-- #container -->
  <!--</div>-->

  <!-- Componentes -->
  <?php
    footer();
  ?>

  <script type="text/javascript">
    getParams();
    getGroups('group1');
    getGroups('group2');
    getGroups('group3');
  </script>

</body>
</html>
