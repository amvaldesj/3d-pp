<?php
/*
 * getPatternsStats.php
 * 
 * Copyright 2014 Alejandro Valdés Jimenez <avaldes@utalca.cl>
 * 
 */

require_once 'lib/common.php';

$type = $_POST['type'];
$path = $_POST['path'];
$step = $_POST['step'];
$radius = $_POST['radius'];
$email = $_POST['email'];
$coverage = $_POST['coverage'];
$diffrmsd = $_POST['diffrmsd'];

switch ($type) {
  case "xray":
    # concatena el comando a ejecutar.
    #$cmd = "python -W ignore ../3d-pp.py " 
    #. $path . "/lista.txt " . $path . " " . $distNeighbor . " " 
    #. $thresholdMax . " " . $GLOBALS['PDBDIR'] . " " . $coverage . " " . $diffAngle . " " . $percSim;
    
    
    $cmd = "python -W ignore ../3d-pp.py ".
      " -fileid " . $path . "/lista.txt" .
      " -dirout " . $path . 
      " -step " . $step . 
      " -radius " . $radius . 
      " -dirpdb " . $GLOBALS['PDBDIR'] .
      " -coverage " . $coverage . 
      " -diffrmsd " . $diffrmsd;
    break;
    
  case "insilico":
    # directorio para alojar los pdbs in silico.
    exec("mkdir -p " . $path . "/pdbs");

    # descomprime y cambia a minúscula archivos pdbs.
    exec("unzip -LL " . $path . "/lista.zip -d " . $path . "/pdbs");
    
    # genera lista.txt con solo IDs. Sin la extensión.
    exec("ls " . $path . "/pdbs/ -1 | sed -e 's/\..*$//' > " . $path . "/lista.txt");

    # concatena el comando a ejecutar.
    ###$cmd = "python -W ignore ../3d-pp.py " . $path . "/lista.txt " . $path . " " . $distNeighbor . " " . $thresholdMax . " " . $path . "/pdbs/" . " " . $coverage . " " . $diffAngle . " " . $percSim;
    $cmd = "python -W ignore ../3d-pp.py " .
      " -fileid " . $path . "/lista.txt" .
      " -dirout " . $path . 
      " -step " . $step . 
      " -radius " . $radius . 
      " -dirpdb " . $path . "/pdbs/" .
      " -coverage " . $coverage . 
      " -diffrmsd " . $diffrmsd;
    break;
}

exec($cmd);

# cambia de directorio
chdir($path);
        
# crea enlaces simbólicos necesarios.
exec("ln -s ../../resultsTemplate.php index.php");
###exec("ln -s ../../resultsTemplateSim.php index2.php");
exec("ln -s ../../lib/ lib");
exec("ln -s ../../js/ js");
exec("ln -s ../../css/ css");
exec("ln -s ../../imgs/ imgs");
exec("ln -s ../../components/ components");
exec("ln -s ../../components/jmol jmol");
exec("ln -s ../../components/jmol/jsmol/j2s/ j2s");
exec("ln -s ../../show3D.php show3D.php");
exec("ln -s ../../show3DAlign.php show3DAlign.php");
exec("ln -s ../../dbstats.php dbstats.php");

switch ($type) {
  case "xray":
    exec("ln -s " . $GLOBALS['PDBDIR'] . " pdbs");
}

$urlResults = $GLOBALS["Host"] . $path;
sendMailNotification($email, $urlResults);

?>
