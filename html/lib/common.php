<?php
require 'PHPMailerAutoload.php';

$Title = "3D-PP";
###$Host = "https://appsbio.utalca.cl/3d-pp/";
$Host = "http://localhost/~amvaldesj/3d-pp/html/";
$USERSMTP = "username@gmail.com";
$PWDSMTP = "....";
$MAILFROM = "3d-pp@gmail.com";
$MAILFROMNAME = "3D-PP";
###$PDBDIR = "/opt/pdbs/";
$PDBDIR = "/home/amvaldesj/public_html/3d-pp/PDBExample/";

function showParams() {
?>
  <span>Server date: </span><label id="date"></label><br>
  <span>Time (H:M:S): </span><label id="time"></label><br>
  <span>The distance to create Grid of Virtual Coordinates (GvC).: </span><label id="step"></label><span>&#8491;</span><br>
  <span>The maximum radius threshold (in Angstrom) for sites search, from the Virtual Coordinates (VC).: </span><label id="radius"></label><span>&#8491;</span><br>
  <span>The maximum difference (in Angstrom) between the RMSDs of the sites (for clustering): </span><label id="diffrmsd"></label>&#8491;<br>
  <!--<span>The minimum percentage of similarity between the sites in a cluster: </span><label id="percSim"></label>%<br>-->
  <hr>
  <span># Proteins successfully processed: </span><label id="nprot"></label><br>
  <span>List: </span><label id="lprot"></label>
  <hr>
  <span># Repeated proteins: </span><label id="nrepeated"></label><br>
  <span>List: </span><label id="lprotr"></label>
  <hr>
  <span># Proteins not processed: </span><label id="nnotprocess"></label><br>
  <span>List: </span><label id="lprotnot"></label>
<?php
}

/*
 * muestra barra de navegación.
 */
function navbar() {
?>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo $GLOBALS['Host']; ?>"><i class='glyphicon glyphicon-home'></i>&nbsp;<?php echo $GLOBALS['Title']; ?></a></li>
        <li><a href="<?php echo $GLOBALS['Host']; ?>"><i class='glyphicon glyphicon-search'></i>&nbsp;Discover</a></li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo $GLOBALS['Host']; ?>about.php" target="_blank"><i class='glyphicon glyphicon-question-sign'></i>&nbsp;About</a></li>
      </ul>
    </div>
  </div>
</nav>
<?php
}

/*
 * parte final de la página
 */
function footer() {
?>
<hr>
<div class="text-center">
  <a href="http://icb.utalca.cl/">School of Bioinformatics Engineering</a> | Department of Bioinformatics | <a href="http://www.utalca.cl/">Universidad de Talca</a> | Talca, Chile
</div>
<div class="bottom text-center">
  <img src="imgs/utalca.png">&nbsp;<img src="imgs/icb.png">
</div>

<?php
}

/*
 * envia correo.
 */
function sendMailNotification($to, $url) {
  $mail = new PHPMailer;
  // Set mailer to use SMTP
  $mail->isSMTP();
  // Specify main and backup SMTP servers
  $mail->Host = 'smtp.gmail.com';
  // Enable SMTP authentication
  $mail->SMTPAuth = true;
  // SMTP username
  $mail->Username = $GLOBALS["USERSMTP"];
  // SMTP password
  $mail->Password = $GLOBALS["PWDSMTP"];
  // Enable TLS encryption, `ssl` also accepted
  $mail->SMTPSecure = 'tls';
  // TCP port to connect to
  $mail->Port = 587;

  $mail->From = $GLOBALS["MAILFROM"];
  $mail->FromName = $GLOBALS["MAILFROMNAME"];
  
  $mail->addReplyTo($GLOBALS["MAILFROMNAME"], $GLOBALS["MAILFROM"]);
  // copia oculta.
  //$mail->addBCC('avaldes@utalca.cl');
  
  // Add a recipient
  $mail->addAddress($to);
  
  // Set email format to HTML
  $mail->isHTML(true);
  
  $mail->Subject = $GLOBALS['Title'] . ' Results';
  $mail->Body    = 'Dear user:<BR>Thanks for using the ' .$GLOBALS['Title']. ' server.<BR>The results of your query can be seen in the folowing link: <a href="' . $url . '">View results</a><BR>The link will be available by 60 days from today.<P> <B>' .$GLOBALS['Title']. ' team';
  $mail->AltBody = 'View results: ' .$url;

  $mail->SMTPOptions = array(
    'ssl' => array(
      'verify_peer' => false,
      'verify_peer_name' => false,
      'allow_self_signed' => true
    )
  );

  if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
  } else {
    echo 'Message has been sent';
  }
}

?>
