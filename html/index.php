<?php
  require_once 'lib/common.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $GLOBALS['Title']; ?></title>
  
  <!-- ############### CSS ############## -->
  <link href="components/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
  

</head>

<body>
  <?php echo navbar(); ?>
  
  <div class="container">
    <div class="row">
      
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <span class="panel-title">Parameters</span>
          </div>
      
          <div class="panel-body">
            <!-- formulario -->
            <form class="form-horizontal" id="formData" enctype="multipart/form-data" method="post" role="form" onSubmit="return run(this);">
              <!-- -->
              <div class="form-group">
                <div class="col-sm-6">
                  <label for="input-pdb">Type</label>
                </div>
                <div class="col-sm-6">
                  <label class="radio-inline"><input type="radio" name="opttype" id="opttype" value="xray" checked="checked">X-Ray</label>
                  <label class="radio-inline"><input type="radio" name="opttype" id="opttype" value="insilico">In Silico</label>
                </div>
              </div>
              
              <!-- -->
              <div class="form-group" id="divTextFile">
                <div class="col-sm-6">
                  <label for="input-pdb">Text file with PDB IDs (<a href="example/list.txt" download>Example</a>)
                  <a href="#" class="tooltipHelp"><i class="glyphicon glyphicon-question-sign"></i>
                  <span>A text file containing the identifiers of the proteins to be processed. Download the sample file for details.</span>
                  </a></label>
                </div>
                <div class="col-sm-2">
                  <input type="file" name="textfile" id="textfile">
                </div>
              </div>
              
              <div class="form-group" id="divZipFile" hidden>
                <div class="col-sm-6">
                  <label for="input-pdb">File zip (<a href="example/HM_cancer.zip" download>Example</a>)
                  <a href="#" class="tooltipHelp"><i class="glyphicon glyphicon-question-sign"></i>
                  <span>A zip file containing the structures to be processed. Download the sample zip file for details.</span></a>
                  </label>
                </div>
                <div class="col-sm-2">
                  <input type="file" name="zipfile" id="zipfile">
                </div>
              </div>
              
              <!-- -->
              <div class="form-group">
                <div class="col-sm-6">
                  <label for="input-max">The distance to create Grid of Virtual Coordinates (GvC).
                  </label>
                </div>
                <div class="col-sm-2">
                  <input class="form-control" type="number" name="step" id="step" value="0.8" min="0" max="3" required step="any">
                </div>
              </div>
              
              <!-- -->
              <div class="form-group">
                <div class="col-sm-6">
                  <label for="input-max">The maximum radius threshold (in Angstrom) for sites search, from the Virtual Coordinates (VC).
                  </label>
                </div>
                <div class="col-sm-2">
                  <input class="form-control" type="number" name="radius" id="radius" value="3" min="1" max="15" required step="any">
                </div>
              </div>
              
              <!-- -->
              <div class="form-group">
                <div class="col-sm-6">
                  <label>Clustering.</label>
                </div>
              
              </div>
              
              <div class="form-group">
                <div class="col-sm-6">
                  <label for="input-grad">The maximum difference (in Angstrom) between the RMSDs of the sites (for clustering)
                  </label>
                </div>
                <div class="col-sm-2">
                  <input class="form-control" type="number" name="diffrmsd" id="diffrmsd" value="0.5" min="0" max="10" required step="any">
                </div>
              </div>
              
              <!-- -->
              <hr>
              <div class="form-group">
                <div class="col-sm-6">
                  <label for="input-email">Email
                  <a href="#" class="tooltipHelp"><i class="glyphicon glyphicon-question-sign"></i>
                  <span>The results will be sent to this email address.</span>
                  </a></label>
                </div>
                <div class="col-sm-6">
                  <input class="form-control" type="email" name="email" id="email" placeholder="Enter email" required>
                </div>
              </div>
              
              <!-- -->
              <div class="form-group">
                <div class="col-sm-6"></div>
                <div class="col-sm-6">
                  <button type="submit" class="btn btn-primary" id="btnCmp" name="btnCmp"><i class="fa fa-search"></i> Discover</button>
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
      
    </div>
  </div>

  <?php 
    footer();
  ?>
  <!-- ############### JS ############## -->
  <script src="components/jquery/jquery-2.1.4.min.js"></script>
  <script src="components/bootstrap/js/bootstrap.min.js"></script>
  <script src="components/spin/spin.min.js"></script>
  <script type="text/javascript" src="js/settings.js"></script>
  <script type="text/javascript" src="js/index.js"></script>
</body>
</html>
