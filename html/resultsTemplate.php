<?php
require_once 'lib/common.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?php echo $GLOBALS['Title']; ?></title>
  
  <!-- ############### CSS ############## -->
  <link href="components/dataTables/jquery.dataTables.css" rel="stylesheet">
  <link href="components/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="components/jqplot/jquery.jqplot.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">

  <!-- ############### JS ############## -->
  <script src="components/jquery/jquery-2.1.4.min.js"></script>
  <script src="components/bootstrap/js/bootstrap.min.js"></script>
  <script src="components/dataTables/jquery.dataTables.min.js"></script>
  
  <script src="components/jqplot/jquery.jqplot.min.js"></script>
  
  <script src="components/jqplot/plugins/jqplot.canvasAxisLabelRenderer.min.js"></script>
  <script src="components/jqplot/plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
    
  <script src="components/jqplot/plugins/jqplot.canvasTextRenderer.min.js"></script>
  
  <script src="components/jqplot/plugins/jqplot.categoryAxisRenderer.js"></script>  
  
  <script src="components/jqplot/plugins/jqplot.bubbleRenderer.min.js"></script>
  
  <script src="components/jqplot/plugins/jqplot.enhancedLegendRenderer.min.js"></script>
  
  <script src="components/jqplot/plugins/jqplot.pointLabels.min.js"></script>  
  
  <script src="components/spin/spin.min.js"></script>
  
  <script type="text/javascript" src="js/results.js"></script>
  <script type="text/javascript" src="js/settings.js"></script>
  <script type="text/javascript" src="js/charts.js"></script>
  <script type="text/javascript" src="js/dbstats.js"></script>
</head>

<body>
  <?php echo navbar(); ?>

  <!-- /.container -->
  <div class="container-fluid"> 
  
  <!--<div class="row"> -->
    <!-- Parameters -->
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <button class="btn btn-primary" data-toggle="collapse" href="#collapse1">View parameters</button>
          <button onclick="showModalGraphStats();" class="btn btn-success">Graph DB Stats</button>
        </div>

        <div id="collapse1" class="panel-collapse collapse on">
          <div class="panel-body">
            <?php showParams(); ?>
          </div>
        </div>
      </div>
    </div>
  <!--</div>-->

  <!--<div class="row">-->
    <!-- Patterns by identity -->
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <span>Patterns by identity</span>
        </div>
        
        <div class="panel-body">
          
          <div id="spin"></div>
          
          <!-- row -->
          <div class="row">
            <div class="col-md-6">
              <div class="panel panel-default">
                <div class="panel-body">
                  
                  <div style="display: inline-block; text-align: right; width: 100%">
                    <label>Search Patterns (RegExp): &nbsp;</label>
                    <input type="text" id="inputFilterPattern">
                  </div>
                  
                  <div class="table-responsive">
                    <table id="table-patterns" class="display table compact nowrap"></table>
                  </div>
                </div>
              </div>
            </div>
            
            <!-- CHART -->
            <div class="col-md-6">
              <div class="panel panel-default">
                <div class="panel-body">
                  <div id="chart" class="table-responsive"></div>
                  <div id="tooltip" style="position:absolute;z-index:99;display:none;"></div>
                </div>
              </div>
            </div>
          </div>
          
          <!-- row -->
          <div class="row">
            <div class="col-md-6">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <span id="titleClusters">Clusters in Pattern </span><b><span id="titleClusterPat"></span></b>
                </div>
                <div class="panel-body">
                  <div class="table-responsive">
                    <table id="table-clusters" class="display table compact nowrap"></table>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <span id="titleSequences">Sites in Cluster </span><b><span id="titleSeqPat"></span></b>
                </div>
                <div class="panel-body">
                  <div class="table-responsive">
                    <table id="table-sites" class="display table compact nowrap"></table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <!-- row -->
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <span>RMSD Stats for Cluster </span><b><span id="titleGraphRMSD"></span></b>
                </div>
                <div class="panel-body">
                  <div>
                    <div id="chartRMSD" class="table-responsive"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>          
          
        </div>
      </div>
    </div>
  
    <!-- Modal align -->
    <div class="modal fade" id="modal-align" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Alignment</h4>
          </div>
        
          <div class="modal-body">
            <div class="panel panel-default">
              <div class="panel-heading">
                <span id="titleAlign">&nbsp;</span></b>
              </div>
                
              <div class="panel-body" style="max-height: 400px;overflow-y: scroll;">
                <canvas id="canvasAlign" width="1" height="1" style="border:0px solid #c3c3c3;">
                  Your browser does not support the HTML5 canvas tag.
                </canvas>
              </div>
            </div>
          </div>
        
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <!-- modal -->
    <div class="modal fade" id="modal-graphstats">
      <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Graph DB Stats</h4>
        </div>
        
        <!-- body modal-->
        <div class="modal-body">
          <div class="panel panel-default">
          <div class="panel-body">
            <label>Size: &nbsp;</label><label id="size"></label><br>
            <label>Nodes/Edges:</label><br>
            <label id="lists"></label>
          </div>
        </div>
        </div>
        
        <!-- footer modal -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success btn-lg" data-dismiss="modal">Close</button>
        </div>
      </div>
      
      </div>
    </div>

  <!-- #container -->
  </div>

  <!-- Componentes -->
  <?php
    footer();
  ?>

  <script type="text/javascript">
    getParams();
    getPatterns();
    getStats();
  </script>
</body>
</html>
