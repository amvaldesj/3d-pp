<?php
require_once 'lib/common.php';
?>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?php echo $GLOBALS['Title']; ?></title>
  
  <!-- ############### CSS ############## -->
  <link href="components/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
  
  <!-- ############### JS ############## -->
  <script src="components/jquery/jquery-2.1.4.min.js"></script>
  <script src="components/bootstrap/js/bootstrap.min.js"></script>
</head>

<body>
  <?php echo navbar(); ?>

  <!--  
  <div class="container">
    <div class="row">
  -->
      <div class="col-md-12">
        <p></p>
        <div class="panel panel-default">
          <div class="panel-heading">
            <span class="panel-title">About</span>
          </div>
      
          <div class="panel-body">
            <p class="text-justify">
            The conservation of amino acids implies in most cases a biological significance. Therefore, 
            several sequence-based initiatives have been successful for instance, to: i) establish sequence 
            evolutionary relationships, ii) detect functional sites and iii) organizes proteins sequences 
            and their annotations. However, these approaches have certain limitations related with the divergent 
            nature of the biological sequences. Indeed, the fact of that the structure of proteins is several 
            times more conserved than their sequence is relevant, and for example, it has been shown that some 
            conserved structural patterns between related proteins do not preserve their sequences. 
            </p>
            <p class="text-justify">
            This situation, together with the increasing availability of structural data (more than 100.000 structures in 
            the Protein Data Bank and more than 3 millions of homology models in the SWISS-MODEL Repository), 
            represents an opportunity to use and develop structure-based methods for the classification, 
            description and searching of tridimensional (3D) amino acid patterns. Although several tools have 
            been developed for these requirements, they usually demand a known query (e.g. orthosteric binding 
            sites, annotated motif, ligands, among others). Nevertheless, for considering all unknown 3D 
            patterns (e.g. allosteric binding sites, putative binding sites), it is precise to have new tools to 
            discover, instead of just describe, search or detect.
            </p>
            <p class="text-justify">
            <b><?php echo $GLOBALS['Title']; ?></b>, is a free access web service to discover all conserved 3D amino acid patterns among 
            a set of protein structures, including those coming from both, X-ray crystallographic experiments 
            and <i>in silico</i> comparative modelling.
            </p>
            
            <p class="text-justify">
            If you have any question about <b><?php echo $GLOBALS['Title']; ?></b>, please send us an email to <a href="mailto:avaldes@utalca.cl">avaldes@utalca.cl</a>
            </p>
          </div>
        </div>
      </div>
    <!--  
    </div>
    
  </div> -->
  
  <?php
    footer();
  ?>
</body>
</html>
