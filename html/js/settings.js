//http://fgnass.github.io/spin.js/
var optsSpin = {
  lines: 10 // The number of lines to draw
, length: 28 // The length of each line
, width: 5 // The line thickness
, radius: 10 // The radius of the inner circle
, scale: 1 // Scales overall size of the spinner
, corners: 1 // Corner roundness (0..1)
, color: '#000' // #rgb or #rrggbb or array of colors
, opacity: 0.25 // Opacity of the lines
, rotate: 0 // The rotation offset
, direction: 1 // 1: clockwise, -1: counterclockwise
, speed: 1 // Rounds per second
, trail: 60 // Afterglow percentage
, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
, zIndex: 2e9 // The z-index (defaults to 2000000000)
, className: 'spinner' // The CSS class to assign to the spinner
, top: '50%' // Top position relative to parent
, left: '50%' // Left position relative to parent
, shadow: false // Whether to render a shadow
, hwaccel: false // Whether to use hardware acceleration
, position: 'absolute' // Element positioning
}

mapCode = {'A':'ALA', 'R':'ARG', 'N':'ASN', 'D':'ASP', 'C':'CYS', 'Q':'GLN', 'E':'GLU',
  'G':'GLY', 'H':'HIS', 'I':'ILE', 'L':'LEU', 'K':'LYS', 'M':'MET', 'F':'PHE', 'P':'PRO',
  'S':'SER', 'T':'THR', 'W':'TRP', 'Y':'TYR', 'V':'VAL'}

mapColor = {'ALA':'#FF0000', 'ARG':'#DF7401', 'ASN':'#9FA40F', 'ASP':'#04B4AE', 'CYS':'#F78181', 'GLN':'#A901DB', 'GLU':'#DF01D7',
  'GLY':'#CFC8C8', 'HIS':'#86B404', 'ILE':'#F4F0B9', 'LEU':'#F8E0F7', 'LYS':'#F7EE74', 'MET':'#BFB0CA', 'PHE':'#FFBF00', 'PRO':'#04B404',
  'SER':'#5882FA', 'THR':'#642EFE', 'TRP':'#848484', 'TYR':'#AEB404', 'VAL':'#8258FA'}
