$(function() {
  $('#inputFilterPattern').on('keyup', function () {

    $('#table-patterns').DataTable()
      .column(1)
      .search(this.value, true, false)
      .draw();
  });
});

function showModalGraphStats() {
  $("#modal-graphstats").modal("show");
}

function getPatternsInGroup(theGroup, group) {
  $('#titlePat' + theGroup).html(" in <b>" + group + "</b>");

  $.getJSON('./json/patterns-' + theGroup + '-' + group + '.json', function(mydata) {
    // carga los patrones en la tabla.
    $('#table-patterns-' + theGroup).DataTable({
      "aaData": mydata,
      "columnDefs": [
        { "title": "Identity pattern", "targets": 0, "searchable": false, "mDataProp": "pattern" },
        { "title": "In Proteins", "targets": 1, "searchable": false, "mDataProp": "enProt", "className": "dt-center"},
        { "title": "% Coverage", "targets": 2, "searchable": false, "mDataProp": "porcent", "className": "dt-center"},
      ],
      "bDestroy": true,
      "scrollX": false,
      "order": [[ 0, "asc" ], [1, "asc" ]],
      "searching": false,
      "search": {
        "regex": true,
        "smart": false
      }
    });
  });
}

function getParams() {
  // carga parámetros
  $.getJSON('./json/params.json', function(mydata) {
    $('#date').html(mydata.date);
    $('#time').html(mydata.time);
    $('#radius').html(mydata.radius);
    $('#clas').html(mydata.clas);
    $('#step').html(mydata.step);
    //$('#coverage').html(mydata.coverage);
    $('#diffrmsd').html(mydata.diffrmsd);
    //$('#percSim').html(mydata.percSim);

    prot = mydata.proteins;
    $('#nprot').html(prot.length);
    var text = '';
    for (i = 0; i < prot.length; i++) {
      text += "<a href='csv/" + prot[i] + ".csv'>" + prot[i] + "</a> ";
    }
    $('#lprot').html(text);
    
    repeat = mydata.repeated;
    $('#nrepeated').html(repeat.length);
    var text = '';
    for (i = 0; i < repeat.length; i++) {
      text += repeat[i] + " ";
    }
    $('#lprotr').html(text);
    
    noproc = mydata.notprocess;
    $('#nnotprocess').html(noproc.length);
    var text = '';
    for (i = 0; i < noproc.length; i++) {
      text += noproc[i] + " ";
    }
    $('#lprotnot').html(text);

  });
}


function setDataInDatatable (data2) {
  // carga los patrones en la tabla.
  var patternsDataTable = $('#table-patterns').dataTable({
    "columnDefs": [
      { "title": "", "targets": 0, "searchable": false, "width": 10, "className": "dt-center", "orderable": false},
      { "title": "Pattern", "targets": 1, "searchable": true},
      { "title": "In Prot", "targets": 2, "searchable": false, "className": "dt-center"},
      { "title": "Not In", "targets": 3, "searchable": false, "className": "dt-center"},
      { "title": "% Coverage", "targets": 4, "searchable": false, "className": "dt-center"},
      { "title": "# Clusters", "targets": 5, "searchable": false, "className": "dt-center"},
      { "title": "# Total sites", "targets": 6, "searchable": false, "className": "dt-center"},
      { "title": "In", "targets": 7, "searchable": false, "visible": false}, //for chart
    ],
    "data": data2,
    "bDestroy": true,
    "scrollX": false,
    "order": [[ 4, "desc" ], [5, "asc"], [6, "desc"]],
    "searching": true,
    
    //"search": {
    //  "regex": true,
    //  "smart": true
    //},
    
    //"sDom": '<"top"i>rt<"bottom"flp><"clear">', //http://legacy.datatables.net/ref#sDom
    "sDom": '<"top"l>t<"bottom"ip><"clear">',
  });
  
  //patternsDataTable.fnClearTable();
}

function getPatterns() {
  var target = document.getElementById('spin')
  var spinner = new Spinner(optsSpin).spin(target);
  var data2 = [];
  
  $.ajax({
    url: './json/patterns.json',
    dataType: 'json',
    async: true,
    success: function(mydata) {
      for (var i=0; i<mydata.length;i++) {

        data2.push([
          "<button type='button' class='btn btn-default btn-xs' " +
          "onclick=\"getClusters('" + mydata[i]["pattern"] + "');\" title='Show clusters'>" +
          "<i class=\"glyphicon glyphicon-align-justify\"><\/i><\/button>",
          mydata[i]["pattern"],
          "<a href='javascript: showList(\"" + mydata[i]["inProteins"] + "\");' title='Show list of proteins where the pattern appears'>" + mydata[i]["enProt"] + "</a>",
          "<a href='javascript: showList(\"" + mydata[i]["notInProteins"] + "\");' title='Show list of proteins where the pattern not appears'>" + mydata[i]["noEnProt"] + "</a>",
          mydata[i]["percentage"],
          mydata[i]["nCluster"],
          mydata[i]["totalSitesCluster"],
          mydata[i]["enProt"]
        ]);
        
      }
      
      setDataInDatatable(data2);
      
      viewChart();
      
      spinner.stop();
    }
  });
}

function showList(list) {
  alert(list);
}

function getGroups(theGroup) {
  var groupDataTable = $('#table-' + theGroup).dataTable({
    "columnDefs": [
      { "title": "", "targets": 0, "searchable": false, "orderable": false},
      { "title": "Similarity pattern", "targets": 1, "searchable": true},
      { "title": "In Proteins", "targets": 2, "searchable": false, "className": "dt-center"},
      { "title": "% Coverage", "targets": 3, "searchable": false, "className": "dt-center"},
      { "title": "# Identity pattern", "targets": 4, "searchable": false, "className": "dt-center"}
    ],
    "bDestroy": true,
    "scrollX": false,
    "order": [[ 3, "desc" ],  [4, "desc" ]],
    "searching": true,
    "search": {
      "regex": true,
      "smart": false
    }
  });

  groupDataTable.fnClearTable();

  $.ajax({
    url: './json/' + theGroup + '.json',
    dataType: 'json',
    async: true,
    success: function(mydata) {
      
      for (var i=0; i<mydata.length;i++) {
        groupDataTable.fnAddData([
          "<button type='button' class='btn btn-default btn-xs' " + 
          "onclick=\"getPatternsInGroup('" + theGroup + "','" + mydata[i]["group"] + "');\" " +
          "title='Show identity patterns in group'>" +
          "<i class=\"glyphicon glyphicon-align-justify\"><\/i><\/button>",
          mydata[i]["group"],
          mydata[i]["enProt"],
          mydata[i]["porcent"],
          mydata[i]["enIdenPattern"]
        ]);
      }
    }
  });
}

function getSites(cluster) {
  $("#titleSeqPat").html(cluster);
  
  var showGrap = true;
  
  var data2 = [];
  $.ajax({
    url: "./json/sites-in-" + cluster + '.json',
    dataType: 'json',
    async: true,
    success: function(mydata) {
      
      if (mydata.length == 1) {
        showGrap = false;
      }
      
      var s1 = [];
      var ticks = [];
      
      for (var i=0; i<mydata.length;i++) {
        data2.push([
          "<button type='button' class='btn btn-default btn-xs' " +
          "onclick=\"window.open('show3D.php?protein=" + mydata[i]["protein"] + 
          "&chain=" + mydata[i]["chain"] + "&site=" + mydata[i]["site"] + "','_blank');\" title='View in jsmol'>" +
          "<i class=\"glyphicon glyphicon-eye-open\"><\/i><\/button>",
          mydata[i]["virtualres"],
          mydata[i]["site"],
          mydata[i]["chain"],
          mydata[i]["protein"],
          mydata[i]["rmsd"],
          mydata[i]["base"],
        ]);
        
        if (mydata[i]["rmsd"] >= 0) {
          s1.push(mydata[i]["rmsd"]);
          ticks.push(mydata[i]["virtualres"]);
        }
      }
      
      var sitesDataTable = $('#table-sites').dataTable({
        "columnDefs": [
          { "title": "", "targets": 0, "searchable": false, "className": "dt-center", "orderable": false},
          { "title": "Site ID", "targets": 1, "searchable": true, "className": "dt-center", "orderable": false},
          { "title": "Site", "targets": 2, "searchable": true, "className": "dt-center dt-body-left"},
          { "title": "Chain", "targets": 3, "searchable": true, "className": "dt-center"},
          { "title": "Protein", "targets": 4, "searchable": true, "className": "dt-center"},
          { "title": "RMSD", "targets": 5, "searchable": false, "className": "dt-center"},      
          { "title": "Base", "targets": 6, "searchable": true, "className": "dt-center"},      
        ],
        "data": data2,
        "bDestroy": true,
        "scrollX": false,
        "order": [[ 5, "asc" ]],
        "searching": true,
        "search": {
          "regex": true,
          "smart": false
        }
      });
  
      //sitesDataTable.fnClearTable();
      
      $('#chartRMSD').empty();
      $("#titleGraphRMSD").html(cluster);
      showBarChart1('chartRMSD', s1, ticks);      
      }
  });
}

function getClusters(pattern) {
  $("#titleClusterPat").html(pattern);
  
  $('#chartRMSD').empty();
  $("#titleGraphRMSD").html("");

  
  $('#table-sites').empty();
  $("#titleSeqPat").html("");

  
  var data2 = [];
  
  $.ajax({
    url: "./json/clusters-in-" + pattern + '.json',
    dataType: 'json',
    async: true,
    success: function(mydata) {
      for (var i=0; i<mydata.length;i++) {
        var dis = " ";
        if (mydata[i]["nSite"]==1) {
          dis = "disabled='disabled' ";
        }
        
        data2.push([
          "<button type='button' class='btn btn-default btn-xs' " + 
            "onclick=\"getSites('" + mydata[i]["cluster"] + "');\" title='Show sites'>" +
            "<i class=\"glyphicon glyphicon-align-justify\"><\/i><\/button>&nbsp;" +
          "<button type='button' class='btn btn-default btn-xs' " + dis +
            "onclick=\"showAlign('" + mydata[i]["cluster"] + "');\" title='Show alignment'>" +
            "<i class=\"glyphicon glyphicon-indent-left\"><\/i><\/button>&nbsp;" +
          "<button type='button' class='btn btn-default btn-xs' " + dis +
            "onclick=\"window.open('show3DAlign.php?cluster=" + mydata[i]["cluster"] + "','_blank');\" " +
            "title='Show 3D alignment'>" +
            "<i class=\"glyphicon glyphicon-eye-open\"><\/i><\/button>",
          mydata[i]["cluster"],
          mydata[i]["nSite"],
          mydata[i]["inProt"],
          mydata[i]["coverage"],
        ]);
      }
      
      var clustersDataTable = $('#table-clusters').dataTable({
        "columnDefs": [
          { "title": "", "targets": 0, "searchable": false, "width": "70", "className": "dt-center", "orderable": false},
          { "title": "Cluster", "targets": 1, "searchable": true},
          { "title": "# Sites", "targets": 2, "searchable": false, "className": "dt-center"},
          { "title": "In Prot", "targets": 3, "searchable": false, "className": "dt-center"},
          { "title": "% Coverage", "targets": 4, "searchable": true, "className": "dt-center"},
        ],
        "data": data2,
        "bDestroy": true,
        "scrollX": false,
        "order": [[ 4, "desc" ]],
        "searching": true,
        "search": {
          "regex": true,
          "smart": false
        },
      });
  
      //clustersDataTable.fnClearTable();
    }
  });

  // foco.
  $('html, body').animate({ scrollTop: $('#titleClusters').offset().top }, 'slow');
}

function getSubPatt() {
  $("#div-align").empty();
  $("#titleAlign").html('&nbsp;');
  nprot = $('#nprot').html();
  sub = $('#inputSubPatt').val();
  t = $('#table-patterns').dataTable();

  // limpia el canvas.
  var c = document.getElementById("canvasAlign");
  var ctx = c.getContext("2d");
  ctx.clearRect(0, 0, c.width, c.height);

  // si subpatrón es vacio.
  if (sub.length <= 0) {
    t.fnFilter('', 1, true, false);
    t.fnDraw();
    $("#subIn").html("");
    alert("Enter subpattern.");
    return;
  }

  // filtra la tabla por el subpatron.
  t.fnFilter(sub, 1, true, false);
  t.fnDraw();

  var listProt = [];
  var totalOcc = 0;
  var totalIn = 0;

  // obtiene solo las filas filtradas de la tabla.
  var rows = $('#table-patterns').dataTable()._('tr', {"filter":"applied"});

  // por cada patrón filtrado trae su archivo json.
  for (var i = 0; i < rows.length; i++) {
    // obtiene sólo el nombre del patrón.
    var patt = String(rows[i]['pattern']);

    $.ajax({
      url: './json/' + patt + '.json',
      dataType: 'json',
      async: false,
      success: function(mydata) {
        totalOcc = totalOcc + mydata.length;

        // obtiene las proteínas donde aparece el patrón.
        for (var j = 0; j < mydata.length; j++) {
          if ($.inArray(mydata[j]['protein'], listProt) == -1) { // no existe en el arreglo.
            listProt.push(mydata[j]['protein']);
          }
        }
      }
    });
  }

  totalIn = listProt.length;

  // muestra resultados.
  //$("#subIn").html('In ' + totalIn + ' of ' + nprot + ' proteins (' + parseFloat((totalIn/nprot)*100).toFixed(2)  + ' %) with ' + totalOcc + ' sequences');
  $("#subIn").html('In ' + totalIn + ' of ' + nprot + ' proteins (' + parseFloat((totalIn/nprot)*100).toFixed(1)  + ' %)');
  
  viewChart();
}

function downloadCSV (cluster) {
  path = "csv/" + cluster + ".csv";
  console.log(path);
  window.open(path);
}
