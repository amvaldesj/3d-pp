//http://wiki.jmol.org/index.php/Jmol_JavaScript_Object/Info
Info = {
  use: "HTML5",
  width: "600",
  height: "500",
  debug: false,
  color: "white",
  j2sPath: "j2s",
  disableJ2SLoadMonitor: false,
  disableInitialConsole: true,
  addSelectionOptions: false,
  allowjavascript: true,
  script: "<set below>"
}

function getApplet(protein, seq, chain, center) {
  pdbfile = "pdbs/" + protein + ".pdb";
  
  if (pdbfile != 'empty') {    
    Info.script = "set antialiasDisplay false;";
    Info.script = Info.script + "load files \"" + pdbfile + "\";";
    Info.script = Info.script + "select " + center + "; center selected; select *;";
    Info.script = Info.script + "hide *; display *:" + chain + "; color gray;";
    Info.script = Info.script + "wireframe on;backbone off;ribbon off;trace off;cartoon off;strands off;meshribbon off;rockets off;cpk off; labels off;";
    Info.script = Info.script + "select " + seq + "; color cpk;";
    Info.script = Info.script + "wireframe 0.2;";
    Info.script = Info.script + "select ligand;";
    Info.script = Info.script + "color gray; spacefill 0.5; zoomto (" + seq + ") 0;";
  
  }
  
  $("#mydiv1").html(Jmol.getAppletHtml("jmolApplet1", Info))
}

function showSite(protein, chain, seq) {
  var aminos = seq.split(":");

  var site = "";
  aminos.forEach (function(res) {
    r = res.substring(0, 3);
    //r = mapCode[r];
    n = res.substring(3, res.length);
    a = r + n;
    site = site + a + ":" + chain + ",";
  });


  site = site.substring(0, site.length - 1);
  center = a + ":" + chain;

  $("#panel-title").html("Protein: <b>" + protein + "</b> Chain: <b>" + chain + "</b> Site: <b>" + seq + "</b>");
  getApplet(protein, site, chain, center);
}

function show3DAlign(cluster) {
  var sites = [];
  console.log(cluster);

  $.ajax ({
    type: "GET",
    url: "json/sites-in-" + cluster + ".json",
    dataType: "json",
    async: true,
    success: function(data) {
      
      for (var i=0; i<data.length; i++) {
        
        if (data[i]["base"] == "yes") {
            siteBase = data[i]["site"] + "," + data[i]["protein"] + "," + data[i]["chain"] + ".pdb";
            sites.push(siteBase);
          } else {
            siteAligned = data[i]["site"] + "," + data[i]["protein"] + "," + data[i]["chain"] + ".pdb-aligned.pdb";
            sites.push(siteAligned);
          }
      }
      
      getApplet3DAlign(sites);
      $("#panel-title").html("Cluster: <b>" + cluster + "</b>");
    }
  });
}

function getApplet3DAlign(sites) {
  var files = " ";
  
  for (var i=0; i< sites.length; i++) {
    files = files + " \" pdbSite/" + sites[i] + "\" ";
  }
  
  Info.script = "set antialiasDisplay false;";
  Info.script = Info.script + "load files " + files + ";";
  Info.script = Info.script + "select All; cpk off;";
  Info.script = Info.script + "model All;";

  $("#mydiv1").html(Jmol.getAppletHtml("jmolApplet1", Info))
}

function downloadPDB(protein, chain, site) {
  window.open("pdbs/" + protein + ".pdb",'_blank');
  window.open("pdbSite/" + protein + "-GA-" + chain + ".pdb",'_blank');
  window.open("pdbSite/" + protein + "-CG-" + chain + ".pdb",'_blank');
  window.open("pdbSite/" + site + "," + protein + "," + chain + ".pdb",'_blank');
}
