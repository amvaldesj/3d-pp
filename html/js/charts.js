/*
 * tooltip for help.
 */
$(function() {
  //if ($.browser.msie && $.browser.version.substr(0,1)<7) {
    $('.tooltipHelp').mouseover(function() {
      $(this).children('span').show();
    }).mouseout(function() {
      $(this).children('span').hide();
    })
  //}
});  


function showBarChart1 (chart, s1, ticks) {
  $.jqplot.config.enablePlugins = true;
         
  var plot1 = $.jqplot('chartRMSD', [s1], {
    seriesDefaults: {
      pointLabels: { 
        show: true,
        formatString: "%#.1f",
      },
      shadow: false,
    },
    axes: {
      xaxis: {
        renderer: $.jqplot.CategoryAxisRenderer,
        tickRenderer: $.jqplot.CanvasAxisTickRenderer,
        ticks: ticks,
        tickOptions: {
          fontSize: '10pt',
          showGridline: true,
          angle: -90,
          pad: 0,
        },
        label: "Site ID"
      },
      //yaxis: {
        //label: "&#8491;",
        //label: "Angstrom",
      //},
    },
    highlighter: { show: false }
  });
}

function showLineGraph(cluster) {
  path = "csv/" + cluster + ".csv";
  $('#chart3').empty();
  
  var allData = [];
  var columns = [];
  var series = [];
  var labels = [];
  var siteBase = "";
  
  $.ajax ({
    type: "GET",
    url: path,
    dataType: "text",
    async: false,
    success: function(data) {
      var allTextLines = data.split(/\r\n|\n/);

      // -1 por la última linea vacia que genera el split.
      for (var i=0; i<allTextLines.length - 1; i++) {

        // encabezado.
        if (i == 0) {
          series.push({renderer:$.jqplot.LineRenderer, color:'green',});
          tmpCol = allTextLines[i].split(',');

          for (j=3; j<tmpCol.length; j++) {
            columns.push(tmpCol[j]);
          }
        } else {
          // los datos.
          series.push({renderer:$.jqplot.LineRenderer, color:'red',});
          var tmpData = [];
          var entries = allTextLines[i].split(',');
          
          for (j=3; j<entries.length; j++) {
            tmpData.push(parseFloat(entries[j]));
          }
          
          allData.push(tmpData);
          labels.push(entries[0].trim() + ' ' + entries[2].trim() + ' ' + entries[1].trim());
          
          // obtiene el primer sitio.
          if (i == 1) {
            siteBase = "Site: " + entries[0].trim() + " Chain: " + entries[2].trim() + " Protein: " + entries[1].trim();
          }
        }
      }
    }
  });
    
  if (allData.length>0) {
    $("#titleClusterGraph").html(cluster + " [" + siteBase + "]");
  
    var plot3 = $.jqplot('chart3', allData, { 
      series: series,
      title:'', 
      seriesDefaults: {
        //highlightMouseDown: true,
        rendererOptions: {
          smooth: true,
          animation: {
            show: false
          },
        },
      },
      //highlighter: {
      //  show: true,
      //},
      /*
      legend: {
        renderer: $.jqplot.EnhancedLegendRenderer,
        show: true,
        rendererOptions: {
          numberRows: 15
        },
        placement: 'outsideGrid',
        labels: labels,
        //location: 'nw',
        //rowSpacing: '0px'
      },*/
      axes: {
        xaxis: {
          renderer: $.jqplot.CategoryAxisRenderer,
          label: 'Angles',
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
          tickRenderer: $.jqplot.CanvasAxisTickRenderer,
          tickOptions: {
            angle: -90,
          },
          ticks: columns   
        },
        yaxis: {
          label: 'Grades',
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer
        }
      }
    }); 
  
    window.onresize = function(event) {
      plot3.replot();
    }
  }
}

function viewChart () {
  // obtiene elementos filtrados.
  var rows = $('#table-patterns').dataTable()._('tr', {"filter":"applied"});
  var arr = [];
  
  // por cada patrón filtrado.
  for (var i = 0; i < rows.length; i++) {
    var pattern = String(rows[i][1]);
    var enProt = parseInt(rows[i][7]);
    var percentage = parseFloat(rows[i][4]);
    var nCluster = parseInt(rows[i][5]);

    var tmp = [nCluster, enProt, percentage, String(pattern)];
    
    arr.push(tmp);
  }

  $('#chart').empty();

  var plot = $.jqplot('chart',[arr], {
    title: {
      text: 'Conserved 3D Patterns Frequency',
    },
    seriesDefaults: {
      renderer: $.jqplot.BubbleRenderer,
      rendererOptions: {
        bubbleAlpha: 0.7,
        highlightAlpha: 0.8,
        showLabels: false,
        autoscalePointsFactor: -0.15,
        autoscaleMultiplier: 0.85,
      },
      shadow: false,
      shadowAlpha: 0.05,
    },
    axes: {
        // options for each axis are specified in seperate option objects.
        xaxis: {
          label: "# Clusters",
          pad: 0,
          labelOptions: {
            fontFamily: 'Helvetica, Georgia, Serif',
            fontSize: '10pt'
          }
        },
        yaxis: {
          renderer:$.jqplot.LogAxisRenderer,
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
          labelOptions: {
            fontFamily: 'Helvetica, Georgia, Serif',
            fontSize: '10pt',
            angle: -90,
          },
          label: "# Proteins",
        }
      }
  });

  $('#chart').bind('jqplotDataHighlight',
    function (ev, seriesIndex, pointIndex, data, radius) {
      var chart_left = $('#chart').offset().left;
      var chart_top = $('#chart').offset().top;
      var x = plot.axes.xaxis.u2p(data[0]);  // convert x axis unita to pixels
      var y = plot.axes.yaxis.u2p(data[1]);  // convert y axis units to pixels

      var color = 'rgb(50%,50%,100%)';
      $('#tooltip').css({left:x+radius+30, top:y-20});
      
      $('#tooltip').html('<span style="font-size:14px;font-weight:bold;color:' +
      color + ';"><a href="javascript: getClusters(\'' + data[3] + '\');">' + data[3] + '</a></span><br />' + '#Clusters: ' + data[0] +
      '<br />' + 'In Protein: ' + data[1] + '<br />' + '%Coverage: ' + data[2]);
      $('#tooltip').show();
    });

  // Bind a function to the unhighlight event to clean up after highlighting.
  $('#chart').bind('jqplotDataUnhighlight',
    function (ev, seriesIndex, pointIndex, data) {
      $('#tooltip').empty();
      $('#tooltip').hide();
    });
  
  //$("#modal-chart").modal("show");
  window.onresize = function(event) {
    plot.replot();
  }
}

function drawTextBG(ctx, txt, x, y, color) {
    y = y - 10;
    ctx.save();
    ctx.font = '10px Arial';
    ctx.textBaseline = 'top';
    ctx.fillStyle = color;
    ctx.fillRect(x, y, 25, parseInt('10px Arial', 10));
    ctx.fillStyle = '#000';
    ctx.fillText(txt, x, y);
    ctx.restore();
}

function showAlign(subpattern) {
  var data = [];
  var minId = 999999;
  var maxId = 0;
  var letters = "";
  var tmpName = "";
  var rows;

  if (subpattern == 'None') {
    tmpName = $('#inputSubPatt').val().toUpperCase();
    // obtiene elementos filtrados de la tabla.
    rows = $('#table-structs').dataTable()._('tr', {"filter":"applied"});
  } else {
    tmpName = subpattern.toUpperCase();
    rows = [{'struct': subpattern}];
  }

  // obtiene letras del subpatron
  letters = tmpName.match(/[a-zA-Z]+/g);
  
  letters3 = [];
  for (var k=0; k < letters.length; k++) {
    letters3.push(mapCode[letters[k]]);
  }
  
  // para controlar que no se repitan sitios.
  var dicUniq = [];

  // por cada patrón filtrado.
  for (var i = 0; i < rows.length; i++) {
    var struct = String(rows[i]['struct']);
    // trae el archivo del patrón para procesarlo.
    $.ajax({
      url: './json/sites-in-' + struct + '.json',
      dataType: 'json',
      async: false,
      success: function(mydata) {

        // obtiene las secuencias del patrón.
        for (var j = 0; j < mydata.length; j++) {
          var seq = mydata[j]['site'];
          var seqSplit = seq.split(":");
          var lres = [];

          for (var k = 0; k < seqSplit.length; k++) {
            residue = seqSplit[k].substring(0, 3);

            // controla que solo considere letras del subpatron.
            //if (letters.indexOf(residue) >= 0) {
            if (letters3.indexOf(residue) >= 0) {
              //idRes = parseInt(seqSplit[k].substring(1));
              idRes = parseInt(seqSplit[k].substring(3));
              lres.push([residue, idRes]);

              if (idRes < minId) {
                minId = idRes;
              } else if (idRes > maxId) {
                maxId = idRes;
              }
            }
          }

          // string que concatena el sitio para validar si ya existe.
          var t = mydata[j]['protein'] + mydata[j]['chain'] + lres;

          // para validar que se agregen a data[] elementos únicos.
          if ($.inArray(t, dicUniq) == -1) { // no existe en el arreglo.
            dicUniq.push(t);
            var l = {"protein": mydata[j]['protein'], "chain": mydata[j]['chain'], "residues": lres};
            data.push(l);
          }
        }
      }
    });
  }

  //ordena los datos por id de proteína.
  data.sort(function(a, b) {
    return a.protein.localeCompare(b.protein);
  });

  nCol = (maxId - minId) - 1;
  nFilas = data.length;

  // controla qué columnas mostrar.
  var flag = new Array (nCol);
  for (var j = 0; j < nCol; j++) {
    flag[j] = 0;
  }

  var matriz = new Array (data.length);
  for (var i = 0; i < nFilas; i++) {
    // crea columnas.
    matriz[i] = new Array(nCol);

    // obtiene la secuenecia.
    residues = data[i]["residues"];

    for ( j=0; j < residues.length; j++ ) {
      amino = data[i]["residues"][j][0];
      idAmino = parseInt(data[i]["residues"][j][1]);
      flag[idAmino] = 1;
      
      matriz[i][idAmino - minId] = amino;
    }
  }

  // calcula la cantidad de columnas a mostrar.
  var xCol = 0;
  for (var x=0; x < flag.length; x++) {
    if (flag[x]==1) 
      xCol = xCol + 1
  }
  
  // dibuja la imagen.
  // obtiene el canvas.
  var c = document.getElementById("canvasAlign");
  var ctx = c.getContext("2d");
  
  // define tamaño del canvas.
  c.width = 100 + xCol * 30;
  c.height = 30 + nFilas * 16;
  
  ctx.font = "10px Arial";
  
  ctx.save();
  ctx.translate(35,30);
  ctx.rotate(-0.5*Math.PI);
  ctx.fillText("ID",0,0);
  ctx.restore();
  
  ctx.save();
  ctx.translate(75,30);
  ctx.rotate(-0.5*Math.PI);
  ctx.fillText("chain",0,0);
  ctx.restore();
  
  // valores iniciales.
  var col = 105;
  var fil = 30;
 
  for ( var i = 0; i < nFilas; i++ ) {

    // muestra fila con IDs.
    if (i==0) {
      for ( var j = minId; j <= maxId; j++ ) {
        if (flag[j]==1) {
          ctx.save();
          ctx.translate(col,fil);
          ctx.rotate(-0.5*Math.PI);
          ctx.fillText(j,0,0);
          ctx.restore();
          
          col = col + 30;
        }
      }
    }

    col = 10;
    fil = fil + 15;
    ctx.fillText(data[i]["protein"],col,fil);
    
    col = col + 60;
    ctx.fillText(data[i]["chain"],col,fil);
    
    col = col + 20;
    
    // dibuja los aminos en las columna que hay al menos un dato.
    for ( var j = 0; j < maxId; j++ ) {
      if (matriz[i][j]) {
        color = mapColor[matriz[i][j]];
        drawTextBG(ctx, matriz[i][j], col, fil, color);
        col = col + 30;
      } else {
        if (flag[j+minId] == 1) {
          drawTextBG(ctx, "   -   ", col, fil, "#fff");
          col = col + 30;
        }
      }
      
    }
  }  

  $("#titleAlign").html(String(data.length) + ' Sites in ' + tmpName);
  $('html, body').animate({ scrollTop: $('#titleAlign').offset().top }, 'slow');
  
  $("#modal-align").modal("show");
}
