$(document).ready(function() {
  /* controla que tipo de archivo se subira */
  $("input[name='opttype']").change(function(){
    type = $("input[name='opttype']:checked").val();
    
    switch(type) {
      case 'xray': 
        $("#divZipFile").hide();
        $("#divTextFile").show();
        break;
        
      case 'insilico':
        $("#divTextFile").hide();
        $("#divZipFile").show();
        break;
    }
  });
  
});

function run(form) {
  type = $("input[name='opttype']:checked").val();
  
  switch(type) {
      case 'xray':
        inputfile = $('#textfile');
        break;
        
      case 'insilico':
        inputfile = $('#zipfile');
        break;
  }

  // verifica selección de archivo.
  if (inputfile.prop('files')[0]) {
      var target = document.getElementById('spinxray')
      var spinnerxray;

    // obtiene nombre de archivo zip.
    file = inputfile.prop('files')[0];

    // crea formulario temporal solo para subir el archivo.
    var form_data = new FormData();
    form_data.append('file', file);
    form_data.append('type', type);

    spinnerxray = new Spinner(optsSpin).spin(target);
    
    // sube el archivo al servidor.
    $.ajax({
      type: 'POST',
      url: 'uploadFile.php',
      dataType: 'json',  // what to expect back from the PHP script, if anything
      async: false,
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      success: function(res) {
        // retorna la ruta a los resultados.
        if (res.status == 'error') {
          spinnerxray.stop();
          alert(res.msg);

        } else if (res.status == 'missing') {
          spinnerxray.stop();
          alert("PDB files not found: " + res.msg);

        } else {
          path = res.path;

          // obtiene distancia de CG vecinos para calculo de átomos fantasma.
          step = $("#step").val();

          // obtiene umbral máximo.
          radius = $("#radius").val();

          // obtiene email.
          email = $("#email").val();
          
          // obtiene coverage
          coverage = 20;
          
          // obtiene diferencia en rmsd
          diffrmsd = $("#diffrmsd").val();
          
          var data = {type: type, step: step, email: email, path: path, radius: radius, coverage: coverage, diffrmsd: diffrmsd};

          // obtiene los patrones.
          $.ajax({
            type: "POST",
            async: true, //No espera por la respuesta
            //async: false, // espera por la respuesta
            url: 'getPatternStats.php',
            data: data,
          });
          spinnerxray.stop();
          
          alert("The link with the results will be sent to your email.");
          //location.href = path;

        }
      },
    });
  } else {
    alert("Select file.");
    return false;
  }

  // return false para que redireccione.
  return false;
}
