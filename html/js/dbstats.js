function getStats() {
  $.ajax({
    url: "./json/dbstats.json",
    dataType: 'json',
    async: true,
    success: function(mydata) {
      $("#size").html(mydata.stats.size);
    
      var types = mydata.types;
      var lists = "<ul>";
      $.each(types, function (index, value) {
        lists += "<li>" + index + "</li>";
        
        lists += "<ul>";
        $.each(value, function (ind, val) {
          lists += "<li>" + ind + ": " + val.split(" ").join(", ") + "</li>";
        });
        lists += "</ul>";
      });
      lists += "</ul>";
      
      $("#lists").html(lists);
    }
  });
}
