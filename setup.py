#!/usr/bin/env python

"""
setup.py file for SWIG
"""

from distutils.core import setup, Extension

extra_args = ['-DVERSION2', '-std=c++11', '-fopenmp', '-O3', '-w', '-march=native', '-pipe']

dddpp_module = Extension('_dddpp',
    sources=['dddpp_wrap.cxx', '3d-pp.cpp', 'utils.cpp', 'chain.cpp', 'site.cpp', 'residue.cpp', 'atom.cpp', 'pattern.cpp', 'rmsd.cpp'],
    swig_opts=['-c++'],
    extra_compile_args=extra_args,
    extra_link_args=['-lgomp', 'kdtree/libkdtree.a'],
)

setup (name = 'dddpp',
    version = '0.1',
    author = "AMVALDESJ",
    description = """Simple swig""",
    ext_modules = [dddpp_module],
    py_modules = ["dddpp"],
)
