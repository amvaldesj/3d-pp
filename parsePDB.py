#!/usr/bin/env python
#-*- coding: utf-8 -*-

from Bio.PDB.PDBParser import PDBParser
import numpy as np

from mchain import Mchain
from pdbSite import PDBSite
  
'''
  Obtiene estructura del archivo PDB.
'''
class ParsePDB(object):
  def __init__(self, pdbID, pdbFile, args):
    self._pdbID = pdbID
    self._pdbFile = pdbFile
    self._chains = []
    
    # obtiene la estructura de la proteína.
    parser = PDBParser()
    self._structure = parser.get_structure(self._pdbID , self._pdbFile)
    
    # obtiene lista de las cadenas.
    lChains = []
    for ch in self._structure.get_chains():
      lChains.append(ch.id)

    # deja una lista con valores únicos.
    lChains = np.unique(lChains)

    # PROCESO ES POR CADA CADENA.
    for chain in lChains:
      # obtiene todos los residuos de la estructura (sólo modelo 0).
      model = self._structure[0]
      
      try: 
        mchain = Mchain(chain, model, args.step, args.radius, self._pdbID, args.dirout)
        self._chains.append(mchain)

        # generate PDB files with ghost atoms and geometrical centers.
        pdbsiteGA = PDBSite(mchain.getChainName(), args.dirout + "/pdbSite/" + self._pdbID + "-GA-" + mchain.getChainName() + ".pdb", mchain.getListResiduesGhostAtom())
      
        pdbsiteGC = PDBSite(mchain.getChainName(), args.dirout + "/pdbSite/" + self._pdbID + "-CG-" + mchain.getChainName() + ".pdb", mchain.getListResiduesGeomCenter())
      except Exception, ex:
        print ex
                
  def getChains(self):
    return self._chains
