#!/usr/bin/env python
# -*- coding: utf-8 -*-

from Bio.PDB.Structure import Structure 
from Bio.PDB.Model import Model 
from Bio.PDB.Chain import Chain 
from Bio.PDB.Residue import Residue
from Bio.PDB.Atom import Atom
from Bio.PDB import NeighborSearch
from Bio.PDB import Selection
import Bio.PDB as PDB

import numpy as np
from numpy import array

from geomcenter import GeomCenter
from msite import Site

class Mchain(object):
  def __init__(self, chain, model, step, radius, pdbID, dirout):
    self._minRes =  4
    self._step = step
    self._radius = radius
    self._chain = chain
    self._model = model
    self._pdbID = pdbID
    self._dirout = dirout
    self._residues = list(self._model[chain].get_residues())
    self._listGeomCenter = []
    self._listResiduesGeomCenter = []
    self._listResiduesGhostAtom = []
    self._listSite = []
    self._idx = 1
    
    # Get all atoms from a chain
    self._atomListChain = Selection.unfold_entities(self._model[chain], 'A')
    
    #
    self._buildListGeomCenter()
    
    if len(self._listGeomCenter)>0:
      self._buildListGhostAtom()
    
  '''
    Genera lista de Atoms centros geométricos.
  '''
  def _buildListGeomCenter(self):
    # crear una nueva estructura (structure) con las coordenadas de los centros geométricos
    myStruct = Structure (0)
    myModel = Model(0)
    myStruct.add(myModel)
    myChain = Chain(self._chain)
    myModel.add(myChain)
    
    for res in self._residues:
      # descarta ligandos y otros (icode='alguna letra')
      if (PDB.is_aa(res)):
        cg = GeomCenter(res)

        # no agrega residuo/átomos sin centro geométrico.
        ###if cg.getGeomCenter() != None:
        if cg.getGeomCenter() is not None:
          myRes = Residue((' ', res.id[1], ' '), res.get_resname(), 0)
          myChain.add(myRes)
          myAtom = Atom ('XX', cg.getGeomCenter(), 0, 1, ' ', ' XX', 0, 'X')
          myRes.add(myAtom)
          self._listGeomCenter.append(myAtom)
          self._listResiduesGeomCenter.append(myRes)

  """
    generate ghostAtom
  """
  def _buildListGhostAtom(self):
    #FIXME: número máximo para ID de residuo es 9999 (limitado por número de culumnas en formato archivo PDB)
    # http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html
    
    # crea un cubo con los mínimos y máximos de las coordenadas.
    xMin = None
    xMax = None
    yMin = None
    yMax = None
    zMin = None
    zMax = None
    
    ##############
    # crea estructura con lista de átomos donde hacer búsquedas.
    #FIXED: debería buscar en los residuos!
    ###ns = NeighborSearch(self._listGeomCenter)
    ns = NeighborSearch(self._atomListChain)
    listSites = []
    ##############
    
    # por cada centro geométrico de la lista.
    for cg in self._listGeomCenter:
      x = cg.get_coord()[0]
      if (xMin == None) or (x < xMin):
        xMin = x
      if (xMax == None) or (x > xMax):
        xMax = x
        
      y = cg.get_coord()[1]
      if (yMin == None) or (y < yMin):
        yMin = y
      if (yMax == None) or (y > yMax):
        yMax = y

      z = cg.get_coord()[2]
      if (zMin == None) or (z < zMin):
        zMin = z
      if (zMax == None) or (z > zMax):
        zMax = z

    xMin = float(round(xMin))
    yMin = float(round(yMin))
    zMin = float(round(zMin))
    xMax = float(round(xMax))
    yMax = float(round(yMax))
    zMax = float(round(zMax))

    step = float(self._step)
    
    # ciclo para crear los átomos fantasma.
    xi = xMin
    while (xi <= xMax):
      yi = yMin
      while (yi <= yMax):
        zi = zMin
        while (zi <= zMax):          
          
          #####################3 buscar sitio inmediatamente.
          # considera el átomo como el centro.
          center = array([xi, yi, zi])
          
          '''
          search(self, center, radius, level='A')
          Return all atoms/residues/chains/models/structures
          that have at least one atom within radius of center.
          What entitity level is returned (e.g. atoms or residues)
          is determined by level (A=atoms, R=residues, C=chains, M=models, S=structures).

          o center - Numeric array
          o radius - float (angstrom)
          o level - char (A, R, C, M, S)
          '''
          # busca a partir de un átomo fantasma los residuos cercanos.
          # Retorna lista de Residue.
          site = ns.search(center, self._radius, "R")
          
          # filtra y deja sólo Aminoácidos.
          tmp = []
          for t in site:
            if (PDB.is_aa(t)):
              tmp.append(t)
          site = tmp

          
          # crea TODOS los átomos fantasma.
          '''
          residueGA = Residue((' ', self._idx, ' '), 'XXX', 0)
          ghostAtom = Atom('XX', array([xi, yi, zi]), 0, 1, ' ', ' XX', 0, 'X')
          residueGA.add(ghostAtom)
          self._listResiduesGhostAtom.append(residueGA)
          self._idx+=1
          '''
          
          # el sitio tiene el mínimo de residuos? crea el átomo fantasma.
          ###FIXME: al parecer, cuando no se limita encuentra más sitios!!
          if len(site) >= self._minRes:
          ###if len(site) >= 0:
            residueGA = Residue((' ', self._idx, ' '), 'XXX', 0)
            ghostAtom = Atom('XX', array([xi, yi, zi]), 0, 1, ' ', ' XX', 0, 'X')          
            residueGA.add(ghostAtom)
            self._listResiduesGhostAtom.append(residueGA)
            self._idx+=1
          
            #FIXED: ordena el sitio por nombre del residuo y por el número (ID) del grupo del residuo.
            ###site = sorted(site, key=self.getKey)
            site = sorted(site, key = lambda x: (x.get_resname(), x.get_id()[1]))
            
            # ya existe este sitio?
            #FIXME: utilizar sólo una lista, eliminar listSites() y usar solo _listSite()
            ###if self.__existsSite(site) == False:
            if site not in listSites:
              listSites.append(site)
              s = Site(site, self._chain, self._pdbID, self._model, ghostAtom, self._dirout)
              self._listSite.append(s)
              
          ###########################
          
          zi+=step  
        yi+=step  
      xi+=step  
  
    del(listSites)
    
  def __existsSite(self, site):
    exists = False
    for s in self._listSite:
      if site == s.getResidues():
        exists = True
        break
    return exists
  
  def getChainName (self):
    return self._chain

  def getResidues (self):
    return self._residues
  
  def getListGeomCenter (self):
    return self._listGeomCenter
  
  def getListResiduesGeomCenter(self):
    return self._listResiduesGeomCenter
  
  def getListResiduesGhostAtom (self):
    return self._listResiduesGhostAtom
  
  def getListSites (self):
    return self._listSite
