/*
 * based on FTPDock.
 * http://www.sbg.bio.ic.ac.uk/docking/ftdock.html
 * processes a protein and returns a vector with its objects Chain.
 */
vector<Chain> parse_pdb(string pdb_id, parameters params);
/**/
void print_chains (vector<Chain> chains);
/* calculates the gometrical centers of residues. */
void calc_geom_center_of_residues(Chain *chain);
/* calculates the max/min coordinates of geometrical centers. */
void calc_max_min_coordinates(Chain *chain);
/**/
void find_sites(Chain *chain, parameters params);
/**/
void merge_patterns_in_chain(Chain *chain, parameters params);
/* adds tridimentional point and associates the residue.id to it.
 * FIXME: adds any type of atom? */
void create_kdtree(Chain *chain);
/**/
list<int> search_neighbors_array(Chain *chain, float *point, float radius);
/* returns a site conformed by the extensions. */
Site calculate_point_extensions (Chain *chain, list<int> neigh_list_central, 
    float *center, float *pleft1, float *pright1, float *pleft2, 
    float *pright2, parameters params);
