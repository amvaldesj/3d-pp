/**/
#include <omp.h>
#include <vector>
#include <list>
#include <set>

// for testing only.
#include <chrono>
#include <ctime>

#include <iostream>
using namespace std;
#include <bits/stdc++.h>
#include "structures.h"
#include "chain.h"
#include "site.h"
#include "pattern.h"
#include "utils.h"

// parse each protein. return a vector with all Chains.
void parses_pdbs_version1(vector<string> pdbs, parameters params, vector<Chain> &all_chains) {
    cout << "parsing pdbs... " << endl;
    auto start = chrono::system_clock::now();
    
    // number of PDBs.
    int n = pdbs.size();
    // store chains per thread.
    vector<Chain> chains_thread[params.nthreads];
    
    // parses groups of PDBs in parallel.
    #pragma omp parallel num_threads(params.nthreads)
    {
        // thread ID.
        int id = omp_get_thread_num();
        
        #pragma omp for schedule(runtime)
        for (int i=0; i<n; i++) {

            #ifdef TRACING
            Extrae_event(100, i+1);
            #endif
            
            // get Chains from pdbs[i].
            vector<Chain> tmp = parse_pdb(pdbs[i], params);
            chains_thread[id].insert(chains_thread[id].end(), tmp.begin(), tmp.end());
            
            #ifdef TRACING
            Extrae_event(100, 0);
            #endif
        }
    }

    // (sequential) insert the chains of each thread in all_chains[].
    for (int id=0; id<params.nthreads; id++) {
        #ifdef TRACING
        Extrae_event(101, id+1);
        #endif
        
        all_chains.insert(all_chains.end(), chains_thread[id].begin(), chains_thread[id].end());

        #ifdef TRACING
        Extrae_event(101, 0);
        #endif
    }
    
    //
    auto end = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = end-start;
    cout << elapsed_seconds.count() << " seconds" << endl;
}

void parses_pdbs_version2(vector<string> pdbs, parameters params, vector<Chain> &all_chains) {
    cout << "parsing pdbs... " << endl;
    auto start = chrono::system_clock::now();
    
    // number of PDBs.
    int n = pdbs.size();
    
    // store chains per thread.
    vector<Chain> chains_thread[params.nthreads];
    
    // parses groups of PDBs in parallel.
    #pragma omp parallel num_threads(params.nthreads)
    {
        // thread ID.
        int id = omp_get_thread_num();
        
        #pragma omp for nowait schedule(runtime)
        for (int i=0; i<n; i++) {
            
            #ifdef TRACING
            Extrae_event(100, i+1);
            #endif
            
            // get Chains from pdbs[i].
            vector<Chain> tmp = parse_pdb(pdbs[i], params);
            chains_thread[id].insert(chains_thread[id].end(), tmp.begin(), tmp.end());
            
            #ifdef TRACING
            Extrae_event(100, 0);
            #endif
        }

        // reduction
        // insert the chains of each thread in all_chains[].
        #pragma omp critical
        {
            #ifdef TRACING
            Extrae_event(101, id+1);
            #endif
            
            all_chains.insert(all_chains.end(), chains_thread[id].begin(), chains_thread[id].end());
            
            #ifdef TRACING
            Extrae_event(101, 0);
            #endif
        }
    }
    
    //
    auto end = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = end-start;
    cout << elapsed_seconds.count() << " seconds" << endl;
}

// process chains.
void process_chains_version1(parameters params, vector<Chain> &all_chains, vector<Pattern> &all_patterns) {
    cout << "processing chains... " << endl;
    auto start = chrono::system_clock::now();
    
    // number of Chains.
    int n_chains = all_chains.size();
    
    // store patterns per thread.
    vector<Pattern> patterns_thread[params.nthreads];
    
    #pragma omp parallel num_threads(params.nthreads)
    {
        // thread ID.
        int id = omp_get_thread_num();
        
        #pragma omp for schedule(runtime)
        for (int c=0; c<n_chains; c++) {
            #ifdef TRACING
            Extrae_event(200, c+1);
            #endif
            
            create_kdtree(&all_chains[c]);

            #ifdef TRACING
            Extrae_event(200, 0);
            #endif

            #ifdef TRACING
            Extrae_event(201, c+1);
            #endif
            
            calc_geom_center_of_residues(&all_chains[c]);

            #ifdef TRACING
            Extrae_event(201, 0);
            #endif

            #ifdef TRACING
            Extrae_event(202, c+1);
            #endif
            
            calc_max_min_coordinates(&all_chains[c]);
            #ifdef TRACING
            Extrae_event(202, 0);
            #endif

            #ifdef TRACING
            Extrae_event(203, c+1);
            #endif
            
            find_sites(&all_chains[c], params);
            #ifdef TRACING
            Extrae_event(203, 0);
            #endif

            #ifdef TRACING
            Extrae_event(204, c+1);
            #endif
            
            // patterns in this chain.
            merge_patterns_in_chain(&all_chains[c], params);
            patterns_thread[id].insert(patterns_thread[id].end(), 
                all_chains[c].patterns.begin(), 
                all_chains[c].patterns.end());

            #ifdef TRACING
            Extrae_event(204, 0);
            #endif
        }
    }

    // (sequential) merge patterns into all_patterns[].
    for (int id=0; id<params.nthreads; id++) {
        #ifdef TRACING
        Extrae_event(300, id+1);
        #endif
        
        merge_all_patterns(patterns_thread[id], all_patterns);
        
        #ifdef TRACING
        Extrae_event(300, 0);
        #endif
    }
    
    //
    auto end = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = end-start;
    cout << elapsed_seconds.count() << " seconds" << endl;
}

void process_chains_version2(parameters params, vector<Chain> &all_chains, set<Pattern> *set_patterns) {
    cout << "processing chains... " << endl;
    auto start = chrono::system_clock::now();
    
    // number of Chains.
    int n_chains = all_chains.size();
    
    // store patterns per thread.
    vector<Pattern> patterns_thread[params.nthreads];
        
    #pragma omp parallel num_threads(params.nthreads)
    {
        // thread ID.
        int id = omp_get_thread_num();
        
        //
        long unsigned int n_patterns;
        std::pair<std::set<Pattern>::iterator,bool> ret;
        std::set<Pattern>::iterator iter;
        
        #pragma omp for nowait schedule(runtime)
        for (int c=0; c<n_chains; c++) {
            #ifdef TRACING
            Extrae_event(200, c+1);
            #endif
            
            create_kdtree(&all_chains[c]);
            
            #ifdef TRACING
            Extrae_event(200, 0);
            #endif

            #ifdef TRACING
            Extrae_event(201, c+1);
            #endif
            
            calc_geom_center_of_residues(&all_chains[c]);

            #ifdef TRACING
            Extrae_event(201, 0);
            #endif

            #ifdef TRACING
            Extrae_event(202, c+1);
            #endif
            
            calc_max_min_coordinates(&all_chains[c]);
            
            #ifdef TRACING
            Extrae_event(202, 0);
            #endif

            #ifdef TRACING
            Extrae_event(203, c+1);
            #endif
            
            find_sites(&all_chains[c], params);
            
            #ifdef TRACING
            Extrae_event(203, 0);
            #endif

            #ifdef TRACING
            Extrae_event(204, c+1);
            #endif
            
            // create patterns in this chain.
            merge_patterns_in_chain(&all_chains[c], params);
            patterns_thread[id].insert(patterns_thread[id].end(), 
                all_chains[c].patterns.begin(), 
                all_chains[c].patterns.end());
            
            #ifdef TRACING
            Extrae_event(204, 0);
            #endif
        }
        
        // reduction. 
        // merge patterns into all_patterns[].
        #pragma omp critical
        {
            #ifdef TRACING
            Extrae_event(300, id+1);
            #endif
            
            //
            n_patterns = patterns_thread[id].size();
            for (long unsigned int p=0; p<n_patterns; p++) {
                ret = set_patterns->insert(patterns_thread[id][p]);
                iter = ret.first;
                
                // false == already exist the pattern true == new pattern.
                if (ret.second == false) { 
                    // pattern found. adds sites to the patterns.
                    iter->sites.insert(iter->sites.end(), 
                        patterns_thread[id][p].sites.begin(), 
                        patterns_thread[id][p].sites.end()
                    );
                    
                    // adds proteins to patterns.
                    iter->in_protein.insert(iter->in_protein.end(), 
                        patterns_thread[id][p].in_protein.begin(), 
                        patterns_thread[id][p].in_protein.end()
                    );
                }
            }
            
            #ifdef TRACING
            Extrae_event(300, 0);
            #endif
        }
    }
    
    // print data
    /*
    for (int id=0; id<params.nthreads; id++) {
        for (int p=0; p<patterns_thread[id].size(); p++) {
            printf("%s %d\n", patterns_thread[id][p].name.c_str(), patterns_thread[id][p].sites.size());
            for (int s=0; s<patterns_thread[id][p].sites.size(); s++) {
                printf("\t%s\n", patterns_thread[id][p].sites[s]->str_site.c_str());
            }
        }
    }*/
    
    //
    auto end = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = end-start;
    cout << elapsed_seconds.count() << " seconds" << endl;
}

//
void parses_and_process_data(vector<string> pdbs, parameters params, set<Pattern> *set_patterns) {
    printf("parsing pdbs and processing chains...\n");
    auto start = chrono::system_clock::now();
    //
    vector<Pattern> patterns_thread[params.nthreads];
    int n = pdbs.size();
    //
    #pragma omp parallel num_threads(params.nthreads)
    {
        #pragma omp for schedule(static)
        for (int i=0; i<n; i++) {
            #ifdef TRACING
            Extrae_event(100, i+1);
            #endif
            //
            vector<Chain> tmp_chains = parse_pdb(pdbs[i], params);
            #ifdef TRACING
            Extrae_event(100, 0);
            #endif

            //
            for (Chain chain: tmp_chains)
            #pragma omp task 
            {
                #ifdef TRACING
                Extrae_event(210, i+1);
                #endif
                
                create_kdtree(&chain);
                calc_geom_center_of_residues(&chain);
                calc_max_min_coordinates(&chain);
                find_sites(&chain, params);
                merge_patterns_in_chain(&chain, params);
                //
                int id = omp_get_thread_num();
                patterns_thread[id].insert(patterns_thread[id].end(), \
                    chain.patterns.begin(), \
                    chain.patterns.end());
                
                #ifdef TRACING
                Extrae_event(210, 0);
                #endif
            }
        }
        
        //
        #pragma omp critical
        {
            pair<std::set<Pattern>::iterator,bool> ret;
            set<Pattern>::iterator iter;
            long unsigned int n_patterns;
            int id = omp_get_thread_num();
            
            #ifdef TRACING
            Extrae_event(300, id+1);
            #endif
            
            //
            n_patterns = patterns_thread[id].size();
            for (long unsigned int p=0; p<n_patterns; p++) {
                ret = set_patterns->insert(patterns_thread[id][p]);
                iter = ret.first;
                
                // false == already exist the pattern true == new pattern.
                if (ret.second == false) { 
                    // pattern found. adds sites to the patterns.
                    iter->sites.insert(iter->sites.end(), 
                        patterns_thread[id][p].sites.begin(), 
                        patterns_thread[id][p].sites.end()
                    );
                    // adds proteins to patterns.
                    iter->in_protein.insert(iter->in_protein.end(), 
                        patterns_thread[id][p].in_protein.begin(), 
                        patterns_thread[id][p].in_protein.end()
                    );
                }
            }
            #ifdef TRACING
            Extrae_event(300, 0);
            #endif
        }
    }
    
    //
    auto end = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = end-start;
    cout << elapsed_seconds.count() << " seconds" << endl;
}


// filter the patterns applying the coverture percentage.
void filter_patterns_version1(parameters params, vector<string> pdbs, vector<Pattern> &all_patterns) {
    auto start = chrono::system_clock::now();
    cout << "filtering patterns... " << endl;
        
    int coverage = params.coverage;
    int pdbssize = pdbs.size();
    vector<Pattern>::iterator new_end;

    new_end = remove_if(
        all_patterns.begin(),
        all_patterns.end(),
        [pdbssize, coverage](Pattern &p) {

            // remove duplicates proteins in the patterns.
            sort(p.in_protein.begin(), p.in_protein.end());
            p.in_protein.erase(
                unique(p.in_protein.begin(), p.in_protein.end()), 
                p.in_protein.end()
            );

            // calculate pattern coverage.
            float pattern_coverage = ((float)p.in_protein.size()/(int)pdbssize)*100;
            p.coverage = pattern_coverage;
            
            //if (coverage < pattern_coverage) \
                cout << p.name << " " << p.coverage << endl;

            return pattern_coverage < coverage;
        }
    );

    // update vector size.
    all_patterns.erase(new_end, all_patterns.end());
    
    //
    auto end = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = end-start;
    cout << elapsed_seconds.count() << " seconds" << endl;
}

void filter_patterns_version2(parameters params, vector<string> pdbs, std::set<Pattern> &set_patterns) {
    auto start = chrono::system_clock::now();
    cout << "filtering patterns... " << endl;
        
    //
    std::set<Pattern>::iterator it;
    
    for (it = set_patterns.begin(); it != set_patterns.end(); /*it++*/) {
        sort(it->in_protein.begin(), it->in_protein.end());
        it->in_protein.erase(
            unique(it->in_protein.begin(), it->in_protein.end()), 
            it->in_protein.end()
        );

        // calculate pattern coverage.
        float pattern_coverage = ((float)it->in_protein.size()/(int)pdbs.size())*100;
        it->coverage = pattern_coverage;
        
        // erase pattern.
        if (it->coverage < params.coverage) {
            it = set_patterns.erase(it);
        } else {
            //cout << it->name << " " << it->coverage << endl;
            ++it;
        }
    }
    
    //
    auto end = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = end-start;
    cout << elapsed_seconds.count() << " seconds" << endl;
}

//
int main(int argc, char **argv) {
    auto start_t = chrono::system_clock::now();
    
    //FIXME: validate parameters.
    parameters params;
    params.step = atof(argv[1]);
    params.radius = atof(argv[2]);
    params.rmsd = atof(argv[3]);
    params.coverage = atof(argv[4]);
    params.extend = atof(argv[5]);
    params.nres = atof(argv[6]);
    params.nthreads = atof(argv[7]);
    params.pdb_list_file = argv[8];
    params.dirpdbs = argv[9];
    params.dirout = argv[10];
  
    //FIXME: remove duplicates ids.
    // read list of pdb files.
    cout << "reading list of pdbs... " << endl;
    auto start = chrono::system_clock::now();
    vector<string> pdbs = gets_pdbs_id_from_file(params.pdb_list_file);
    auto end = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = end-start;
    cout << elapsed_seconds.count() << " seconds" << endl;
    
    //
    if (pdbs.empty() == true) {
        cout << "No pdbs to process..." << endl;
    } else {
        //
        #ifdef VERSION1
            // vector to store all Patterns with its Clusters and Sites.
            vector<Pattern> all_patterns;
            // vector to store all Chains.
            vector<Chain> all_chains;
            // parses pdb files.
            parses_pdbs_version1(pdbs, params, all_chains);
            // process chains.
            process_chains_version1(params, all_chains, all_patterns);
        
        #elif VERSION2
            set<Pattern> set_patterns;
            // vector to store all Chains.
            vector<Chain> all_chains;
            // parses pdb files.
            parses_pdbs_version2(pdbs, params, all_chains);
            // process chains.
            process_chains_version2(params, all_chains, &set_patterns);
        
        #elif VERSION3
            // parses pdb and process chains.
            set<Pattern> set_patterns;
            parses_and_process_data(pdbs, params, &set_patterns);
        #endif

        //
        #ifdef TRACING
        Extrae_event(301, 1);
        #endif
        
        // filtering patterns.
        #if defined(VERSION2) || defined(VERSION3)
            filter_patterns_version2(params, pdbs, set_patterns);
        #else
            filter_patterns_version1(params, pdbs, all_patterns);
        #endif

        #ifdef TRACING
        Extrae_event(301, 0);
        #endif

        // clustering sites.
        #if defined(VERSION2) || defined(VERSION3)
            
            /*
            for (set<Pattern>::iterator it = set_patterns.begin(); it != set_patterns.end(); ++it) {
                for (int s=0; s<it->sites.size(); s++) {
                    cout << "site: " << it->sites[s]->str_site << endl;
                }
            }*/
        
        
        
            cluster_sites_version2(params, set_patterns);
        #else
            cluster_sites_version1(params, all_patterns);
        #endif

        // save results.
        #ifdef TRACING
        Extrae_event(500, 1);
        #endif

        #if defined(VERSION2) || defined(VERSION3)
            save_data_to_folder_version2(set_patterns, params, pdbs);
        #else
            save_data_to_folder_version1(all_patterns, params, pdbs);
        #endif
        
        save_parameters(params, pdbs);

        #ifdef TRACING
        Extrae_event(500, 0);
        #endif
    }
    
    //
    auto end_t = chrono::system_clock::now();
    elapsed_seconds = end_t-start_t;
    cout << "total time: " << elapsed_seconds.count() << " seconds" << endl;

    return 0;   
}
